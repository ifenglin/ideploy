#!/bin/bash
# I-Feng Lin, May 28th 2013
# ES, PCI, Websphere, HadoopOps, Web Intelligence, and Unix distributed deployment script.
# Assumes ssh access are already working.
### You may modify the variables for your use. ###

# remote splunk settings
SPLUNK_HOME="/opt/centralsite/splunk"
HTTP_PORT="8000" # Alternatively, change it with options in command.
USER=root

# Below are for the script to fecth app install files
# If you enable option -f to upload a install file, the variables are not used.
# sc-build server address
DEPLOY_SCBUILD_URL="http://sc-build.sv.splunk.com:8081"

# If you want to install a different version, change them here.
DEPLOY_ES_URL="$DEPLOY_SCBUILD_URL/ES/releases/2.4.x/2.4.0/"
DEPLOY_HADOOPOPS_URL="$DEPLOY_SCBUILD_URL/hadoopops/splunk_for_hadoopops/1.1.x/1.1.1/RC4/"
DEPLOY_PCI_URL="$DEPLOY_SCBUILD_URL/PCI/releases/2.0.x/2.0.0/"
DEPLOY_WEBINTELLIGENCE_URL="$DEPLOY_SCBUILD_URL/webintelligence/splunk_for_webintelligence/latest/"
DEPLOY_WEBSPHERE_URL="$DEPLOY_SCBUILD_URL/splunkwas/splunk_app_was/2.0.x/2.0.1/"
DEPLOY_UNIX_URL="$DEPLOY_SCBUILD_URL/unix/splunk_for_nix/4.6/"

# Install file extentions on sc-build server
DEPLOY_ES_ENTENTION=".spl"
DEPLOY_HADOOPOPS_ENTENTION=".zip"
DEPLOY_PCI_ENTENTION=".spl"
DEPLOY_UNIX_ENTENTION=".spl"
DEPLOY_WEBINTELLIGENCE_ENTENTION=".zip"
DEPLOY_WEBSPHERE_ENTENTION=".tar.gz"

### Do not modify the variables below here unless you know what they are. ###

LOGFILE_PREFIX=$PWD/logs/deploy

SSH="ssh -o StrictHostKeyChecking=no"
SCP="scp -pr -o StrictHostKeyChecking=no"

# a log wrapper
run() {
    LOGFILE=$1
    LINE=($@)
    COMMAND=${LINE[@]:1}
    echo "### CMD: \"$COMMAND\", PWD: $PWD" >> $LOGFILE
    if [ "$2" != "python" ] ; then
        $COMMAND >> $LOGFILE 2>&1
    else
        $COMMAND
    fi
}

update_idx_hosts() {
    SPLUNK_CMD="$SPLUNK_HOME/bin/splunk --accept-license"
    SPLUNK_CMD_ENV="export SPLUNK_HOME=$SPLUNK_HOME ; $SPLUNK_CMD"
    #rename arguments
    DEPLOY_HOST=$1
    APP_NAME=$2
    LOGFILE="$LOGFILE_PREFIX-$APP_NAME-on-$DEPLOY_HOST.log"
    #clean old apps
    OLD_APPS_ES="\
        $SPLUNK_HOME/etc/apps/DA-ESS* \
        $SPLUNK_HOME/etc/apps/SA-AccessProtection \
        $SPLUNK_HOME/etc/apps/SA-AuditAndDataProtection \
        $SPLUNK_HOME/etc/apps/SA-CommonInformationModel \
        $SPLUNK_HOME/etc/apps/SA-EndpointProtection \
        $SPLUNK_HOME/etc/apps/SA-Eventgen \
        $SPLUNK_HOME/etc/apps/SA-IdentityManagement \
        $SPLUNK_HOME/etc/apps/SA-NetworkProtection \
        $SPLUNK_HOME/etc/apps/SA-ThreatIntelligence \
        $SPLUNK_HOME/etc/apps/SA-Utils \
        $SPLUNK_HOME/etc/apps/SplunkEnterpriseSecurityInstaller \
        $SPLUNK_HOME/etc/apps/SplunkEnterpriseSecuritySuite \
        $SPLUNK_HOME/etc/apps/TA-airdefense \
        $SPLUNK_HOME/etc/apps/TA-alcatel \
        $SPLUNK_HOME/etc/apps/TA-bluecoat \
        $SPLUNK_HOME/etc/apps/TA-cef \
        $SPLUNK_HOME/etc/apps/TA-fireeye \
        $SPLUNK_HOME/etc/apps/TA-flowd \
        $SPLUNK_HOME/etc/apps/TA-fortinet \
        $SPLUNK_HOME/etc/apps/TA-ftp \
        $SPLUNK_HOME/etc/apps/TA-ip2location \
        $SPLUNK_HOME/etc/apps/TA-juniper \
        $SPLUNK_HOME/etc/apps/TA-mcafee \
        $SPLUNK_HOME/etc/apps/TA-ncircle \
        $SPLUNK_HOME/etc/apps/TA-nessus \
        $SPLUNK_HOME/etc/apps/TA-nmap \
        $SPLUNK_HOME/etc/apps/TA-oracle \
        $SPLUNK_HOME/etc/apps/TA-ossec \
        $SPLUNK_HOME/etc/apps/TA-paloalto \
        $SPLUNK_HOME/etc/apps/TA-rsa \
        $SPLUNK_HOME/etc/apps/TA-sav \
        $SPLUNK_HOME/etc/apps/TA-sep \
        $SPLUNK_HOME/etc/apps/TA-snort \
        $SPLUNK_HOME/etc/apps/TA-sophos \
        $SPLUNK_HOME/etc/apps/TA-splunk \
        $SPLUNK_HOME/etc/apps/TA-tippingpoint \
        $SPLUNK_HOME/etc/apps/TA-trendmicro \
        $SPLUNK_HOME/etc/apps/TA-websense \
        $SPLUNK_HOME/etc/apps/sideview_utils \
    "
    OLD_APPS_PCI="$SPLUNK_HOME/etc/apps/DA-PCI-* \
        $SPLUNK_HOME/etc/apps/SA-AccessProtection \
        $SPLUNK_HOME/etc/apps/SA-AuditAndDataProtection \
        $SPLUNK_HOME/etc/apps/SA-CommonInformationModel \
        $SPLUNK_HOME/etc/apps/SA-EndpointProtection \
        $SPLUNK_HOME/etc/apps/SA-Eventgen \
        $SPLUNK_HOME/etc/apps/SA-IdentityManagement \
        $SPLUNK_HOME/etc/apps/SA-NetworkProtection \
        $SPLUNK_HOME/etc/apps/SA-ThreatIntelligence \
        $SPLUNK_HOME/etc/apps/SA-Utils \
        $SPLUNK_HOME/etc/apps/SplunkPCIComplianceSuite \
        $SPLUNK_HOME/etc/apps/SplunkPCIComplianceSuiteInstaller \
        $SPLUNK_HOME/etc/apps/TA-airdefense \
        $SPLUNK_HOME/etc/apps/TA-alcatel \
        $SPLUNK_HOME/etc/apps/TA-bluecoat \
        $SPLUNK_HOME/etc/apps/TA-cef \
        $SPLUNK_HOME/etc/apps/TA-fireeye \
        $SPLUNK_HOME/etc/apps/TA-flowd \
        $SPLUNK_HOME/etc/apps/TA-fortinet \
        $SPLUNK_HOME/etc/apps/TA-ftp \
        $SPLUNK_HOME/etc/apps/TA-ip2location \
        $SPLUNK_HOME/etc/apps/TA-juniper \
        $SPLUNK_HOME/etc/apps/TA-mcafee \
        $SPLUNK_HOME/etc/apps/TA-ncircle \
        $SPLUNK_HOME/etc/apps/TA-nessus \
        $SPLUNK_HOME/etc/apps/TA-nmap \
        $SPLUNK_HOME/etc/apps/TA-oracle \
        $SPLUNK_HOME/etc/apps/TA-ossec \
        $SPLUNK_HOME/etc/apps/TA-paloalto \
        $SPLUNK_HOME/etc/apps/TA-rsa \
        $SPLUNK_HOME/etc/apps/TA-sav \
        $SPLUNK_HOME/etc/apps/TA-sep \
        $SPLUNK_HOME/etc/apps/TA-snort \
        $SPLUNK_HOME/etc/apps/TA-splunk \
        $SPLUNK_HOME/etc/apps/TA-trendmicro \
        $SPLUNK_HOME/etc/apps/sideview_utils 
    "
    #Splunk_TA_nix cannot be on the instance at the same time; remove it.
    OLD_APPS_UNIX="\
        $SPLUNK_HOME/etc/apps/Splunk_TA_nix\
        $SPLUNK_HOME/etc/apps/TA-nix\
        $SPLUNK_HOME/etc/apps/unix\
    "
    OLD_APPS_HADOOPOPS="\
        $SPLUNK_HOME/etc/apps/SA-HadoopOps\
        $SPLUNK_HOME/etc/apps/splunk_for_hadoopops\
        $SPLUNK_HOME/etc/apps/Splunk_TA_hadoopops\
    "
    OLD_APPS_WEBSPHERE="\
        $SPLUNK_HOME/etc/apps/splunk_app_was\
    "
    OLD_APPS_WEBINTELLIGENCE="\
        $SPLUNK_HOME/etc/apps/SA-Webintelligence\
        $SPLUNK_HOME/etc/apps/splunk_for_webintelligence\
    "
    run $LOGFILE date
    run $LOGFILE $SSH $USER@$DEPLOY_HOST $SPLUNK_CMD_ENV stop
    #remove old apps
    if [ "$APP_NAME" = "es" ] ; then
        APP_URL=$DEPLOY_ES_URL
        APP_EXTENSION=$DEPLOY_ES_ENTENTION
        run $LOGFILE $SSH $USER@$DEPLOY_HOST rm -r -f $OLD_APPS_ES
    elif [ "$APP_NAME" = "pci" ] ; then
        APP_URL=$DEPLOY_PCI_URL
        APP_EXTENSION=$DEPLOY_PCI_ENTENTION
        run $LOGFILE $SSH $USER@$DEPLOY_HOST rm -r -f $OLD_APPS_PCI
    elif [ "$APP_NAME" = "unix" ] ; then
        APP_URL=$DEPLOY_UNIX_URL
        APP_EXTENSION=$DEPLOY_UNIX_ENTENTION
        run $LOGFILE $SSH $USER@$DEPLOY_HOST rm -r -f $OLD_APPS_UNIX
    elif [ "$APP_NAME" = "hadoopops" ] ; then
        APP_URL=$DEPLOY_HADOOPOPS_URL
        APP_EXTENSION=$DEPLOY_HADOOPOPS_ENTENTION
        run $LOGFILE $SSH $USER@$DEPLOY_HOST rm -r -f $OLD_APPS_HADOOPOPS
    elif [ "$APP_NAME" = "websphere" ] ; then
        APP_URL=$DEPLOY_WEBSPHERE_URL
        APP_EXTENSION=$DEPLOY_WEBSPHERE_ENTENTION
        run $LOGFILE $SSH $USER@$DEPLOY_HOST rm -r -f $OLD_APPS_WEBSPHERE
    elif [ "$APP_NAME" = "webintelligence" ] ; then
        APP_URL=$DEPLOY_WEBINTELLIGENCE_URL
        APP_EXTENSION=$DEPLOY_WEBINTELLIGENCE_ENTENTION
        run $LOGFILE $SSH $USER@$DEPLOY_HOST rm -r -f $OLD_APPS_WEBINTELLIGENCE
    fi
    #create stage dir
    STAGE_DIR="stage/"
    run $LOGFILE $SSH $USER@$DEPLOY_HOST mkdir $STAGE_DIR
    if $UPDATE_LIC ; then
        #update licenses
        run $LOGFILE $SCP $LIC_FILE_NAME $USER@$DEPLOY_HOST:$STAGE_DIR/
        run $LOGFILE $SSH $USER@$DEPLOY_HOST $SPLUNK_CMD_ENV add licenses $STAGE_DIR/$LIC_FILE_NAME -auth admin:changeme
    fi
    if $ENABLE_LISTEN_PORT ; then
    #add listen port
        run $LOGFILE $SSH $USER@$DEPLOY_HOST $SPLUNK_CMD_ENV enable listen -port $LISTEN_PORT -auth admin:changeme
    fi
    #declare pkg locations the cmds to retrieve them
    APP_FETCH_CMD="wget -nv --no-directories -r -l 1 --no-parent -A $APP_EXTENSION $APP_URL -P $STAGE_DIR"
    
    #For ES and PCI, fetch and install sideview-utils
    if [ "$APP_NAME" = "es" ] || [ "$APP_NAME" = "pci" ] ; then
        SIDEVIEW_URL="http://sc-build.sv.splunk.com:8081/sideview_utils/1.3.5/"
        SIDEVIEW_FETCH_CMD="wget -nv --no-directories -r -l 1 --no-parent -A .tar.gz $SIDEVIEW_URL -P $STAGE_DIR"
        run $LOGFILE $SSH $USER@$DEPLOY_HOST $SIDEVIEW_FETCH_CMD
        run $LOGFILE $SSH $USER@$DEPLOY_HOST gzip -d "$STAGE_DIR/sideview_utils.tar.gz"
        run $LOGFILE $SSH $USER@$DEPLOY_HOST tar -xvf "$STAGE_DIR/sideview_utils.tar -C $SPLUNK_HOME/etc/apps/"
        run $LOGFILE $SSH $USER@$DEPLOY_HOST rm -f "$STAGE_DIR/sideview_utils.*"
        #run $LOGFILE $SSH $USER@$DEPLOY_HOST mv -f "$STAGE_DIR/sideview_utils $SPLUNK_HOME/etc/apps/"
    fi
    
    
    #get install file
    if [ $UPLOAD_APP_FILE == false ] ; then
        #fetch install from sc-build server
        run $LOGFILE $SSH $USER@$DEPLOY_HOST $APP_FETCH_CMD
    else
        #upload install file
        run $LOGFILE $SCP $INSTALL_APP_FILE $USER@$DEPLOY_HOST:$STAGE_DIR/
        if [[ $INSTALL_APP_FILE == *.spl ]] ; then
            APP_EXTENSION=".spl"
        elif [[ $INSTALL_APP_FILE == *.zip ]] ; then
            APP_EXTENSION=".zip"
        elif [[ $INSTALL_APP_FILE == *.tar.gz ]] ; then
            APP_EXTENSION=".tar.gz"
        else
            echo "Unknown install file type. Upload .spl, .zip, or .tar.gz file."
            exit 1
        fi
    fi
    #install App
    run $LOGFILE $SSH $USER@$DEPLOY_HOST $SPLUNK_CMD_ENV start
    if [ "$APP_EXTENSION" = ".spl" ] ; then 
        run $LOGFILE $SSH $USER@$DEPLOY_HOST $SPLUNK_CMD_ENV install app "$STAGE_DIR/*$APP_EXTENSION" -update true -auth admin:changeme
    elif [ "$APP_EXTENSION" = ".zip" ] ; then
        #unzip should result in etc/ folder. You have to change it if the app is packaged otherwise 
        run $LOGFILE $SSH $USER@$DEPLOY_HOST unzip $STAGE_DIR/*$APP_EXTENSION -d $SPLUNK_HOME/
    elif [ "$APP_EXTENSION" = ".tar.gz" ] ; then
        #tar -xvf should result in etc/ folder. You have to change it if the app is packaged otherwise 
        run $LOGFILE $SSH $USER@$DEPLOY_HOST gzip -d $STAGE_DIR/*tar.gz
        run $LOGFILE $SSH $USER@$DEPLOY_HOST tar -xvf $STAGE_DIR/*.tar -C $SPLUNK_HOME/etc/apps/
    fi
    
    run $LOGFILE $SSH $USER@$DEPLOY_HOST rm -r -f $STAGE_DIR
    run $LOGFILE $SSH $USER@$DEPLOY_HOST $SPLUNK_CMD_ENV restart
    if [ "$APP_NAME" = "es" ] || [ "$APP_NAME" = "pci" ] ; then
        run $LOGFILE python $PWD/install.py $APP_NAME $HTTP_PORT $DEPLOY_HOST
        run $LOGFILE $SSH $USER@$DEPLOY_HOST mkdir $SPLUNK_HOME/etc/apps/SA-Eventgen/local
        run $LOGFILE $SSH $USER@$DEPLOY_HOST cp $SPLUNK_HOME/etc/apps/SA-Eventgen/default/inputs.conf $SPLUNK_HOME/etc/apps/SA-Eventgen/local
        run $LOGFILE $SSH $USER@$DEPLOY_HOST cp $SPLUNK_HOME/etc/apps/SA-Eventgen/default/eventgen.conf $SPLUNK_HOME/etc/apps/SA-Eventgen/local
        run $LOGFILE $SSH $USER@$DEPLOY_HOST $SPLUNK_CMD_ENV enable app SA-Eventgen -auth admin:changeme
        run $LOGFILE $SSH $USER@$DEPLOY_HOST $SPLUNK_CMD_ENV restart
        echo "$DEPLOY_HOST: Starting Selenium to test..."
        run $LOGFILE export DEPLOY_HOST=$DEPLOY_HOST; export HTTP_PORT=$HTTP_PORT; python $PWD/webdriver/test_$APP_NAME.py
    else
        run $LOGFILE export DEPLOY_HOST=$DEPLOY_HOST; export HTTP_PORT=$HTTP_PORT; python $PWD/webdriver/configure_$APP_NAME.py
    fi
    echo "$DEPLOY_HOST: Installtion Complete. If there is no failure, $APP_NAME is good to go!"
}

main() {
    # update the idx hosts
    echo "Deploying $APP_NAME on $IDX_HOSTS...( This takes a couple of minutes. )"
    for i in $IDX_HOSTS ; do
        mkdir -p $PWD/logs
        rm "$LOGFILE_PREFIX-$APP_NAME-on-$i.log" 2> /dev/null
        #run $i echo updating host: $i
        update_idx_hosts $i $APP_NAME &
    done
    wait $!
    
    echo "Processes comeplete. Check logs for deployment results."
    exit 0
}

UPDATE_LIC=false
ENABLE_LISTEN_PORT=false
UPLOAD_APP_FILE=false
while [[ $1 = -* ]]; do
    PARAM=`echo $1 | awk -F= '{print $1}'`
    VALUE=`echo $1 | awk -F= '{print $2}'`
    case $PARAM in
        -l | --licence)
            UPDATE_LIC=true
            LIC_FILE_NAME=$2
            ;;
        -r | --listen-port)
            ENABLE_LISTEN_PORT=true
            LISTEN_PORT=$2
            ;;
        -p | --port)
            HTTP_PORT=$2
            ;;
        -f | --file)
            UPLOAD_APP_FILE=true
            INSTALL_APP_FILE=$2
            ;;
        -h | --help)
            echo 'This script deploys ES, PCI, Websphere, HadoopOps, Web Intelligence, and Unix.'
            echo 'To run the script, run'
            echo './deploy_es.sh <OPTIONS> (es|pci|websphere|hadoopops|webintelligence|unix) <HOST> '
            echo 'you can enter a list of hosts'
            echo 'Sample command: '
            echo '    ./deploy.sh -l 5TB.lic -p 8001 -f es.spl es qasus-centos-5 qasus-centos-6'
            echo ''
            echo 'Options:'
            echo '    -l | --licence <LICENSE FILE>   :  update license'
            echo '    -r | --listen-port <PORT>       :  add a listen/receiving port'
            echo "    -p | --port <PORT>              :  set http port (default $HTTP_PORT)"
            echo '    -f | --file <APP INSTALL FILE>  :  upload app install file'
            echo '    -h | --help                     :  Help Menu'
            echo ''
            echo 'Note: ES, PCI and Websphere supports automatic installation.'
            echo 'HadoopOps, Web Intelligence, and Unix supports semi-automatic installation.'
            echo '(you need to click when you see hints.)'

            exit 1
            ;;
        *)
            echo "ERROR: unknown parameter \"$PARAM\""
            exit 1
            ;;
    esac
        shift 2
done

ARGV=($@)

#if <HOST> is defined, it gives a list of hosts
if [ ${ARGV[1]} = non-cluster-indexers ] ; then
    IDX_HOSTS=" \
    qasus-centos-5 \
    qasus-centos-6 \
    qasus-centos-7 \
    qasus-centos-8 \
    qasus-centos-9 "
else
    IDX_HOSTS=${ARGV[@]:1}
fi
APP_NAME=${ARGV[0]}

# go...
main
