curl(['../common/envs', 'domReady!'], function(envs) {
    var config = envs.config,
        core = envs.core,
        module_name = 'CFScrollerX',
        deps = [
            "js!appserver/static/d3.v2.min.js",
            "js!appserver/static/crossfilter.union.min.js",
            "js!appserver/static/custom.js",
            "js!modules/CFScrollerX/CFScrollerX.js",
            "js!modules/CFScrollerX/scrollerx.js",
            "js!my/data.js",
            "css!appserver/modules/CFScrollerX/CFScrollerX.css"
        ];

    var context, dataset;

    config.paths.my = 'CFScrollerX';
    config.paths.appserver = config.paths.splunk + '/etc/apps/splunk_for_nix/appserver/';
    config.paths.modules = config.paths.appserver + '/modules/';

    curl(config, core)
        .next(deps)
        .then(
            function() {
                setup();
                do_test();
            },
            function(ex) {
                console.log(ex);
            }
        );

    function setup() {
        // mock $script, include them directly in the above
        var $script = function() {};
        
        // inject our own context directly
        context = context_from_object(my_context);
        
        dataset = crossfilter(mydata);
        
        mydata.forEach(function(d) { d.date = new Date(d.date); });
        
        context.set('dataset', dataset);
        context.set('controller', 'controller');
        context.set('namespace', 'unixcf');
        context.set('unixcf', {time_of_day: unixcf.time_of_day,
          date: unixcf.date,
          date_name: unixcf.date_name});
        
        Splunk.Module.loadParams = {
              'top': {
                  width: "320",
                  height: "480",
                  expandWidth:"900",
                  expandHeight: "600"
              }
        };
        
        Splunk.Module.CFScrollerX = $.klass(Splunk.Module.CFScrollerX, {
            getContext: function() { return context; }
        });
        
        var target = $('#top');
        $('#top').attach(Splunk.Module.CFScrollerX, document.getElementById('top'));
        
        // test CFScrollerX.onContextChange
        target.triggerHandler('ContextChange');
    }
    
    // TODO: verify actual data is correct and stack location matches too
    // TODO: restructure tests
    // TODO: separate CFScrollerX and scrollerx
    function do_test() {
        var dimension = unixcf.time_of_day.dimension,
            group = unixcf.time_of_day.group,
            hour = dataset.dimension(dimension),
            hours = hour.group(group),
            all = hour.top(Infinity),
            snapholder = $('#snapshots_holder');
    
        QUnit.config.reorder = false;
    
        asyncTest('default', function() {
            setTimeout(function() {
                verify_scroller(all);
                start();
            }, 500);
        });
    
        asyncTest('scroll prev', function() {
            var evt = document.createEvent('MouseEvents'),
                node = $('.arrow-prev'),
                offset = node.offset();
    
            evt.initMouseEvent("click", true, true, window, 0,
                offset.left, offset.top, node.width() / 2, node.height() / 2,
                false, false, false, false, 0, null);
    
            node[0].dispatchEvent(evt);
            setTimeout(function() {
                verify_scroller(all, all.length - 1);
                start();
            }, 500);
        });
    
        asyncTest('scroll next', function() {
            var evt = document.createEvent('MouseEvents'),
                node = $('.arrow-next'),
                offset = node.offset();
    
            evt.initMouseEvent("click", true, true, window, 0,
                offset.left, offset.top, node.width() / 2, node.height() / 2,
                false, false, false, false, 0, null);
    
            node[0].dispatchEvent(evt);
            setTimeout(function() {
                verify_scroller(all, all.length);
                start();
            }, 500);
        });
    
        asyncTest('scroll (jump)', function() {
            var target = all.length - 5;
            
            $('.scrollerx').triggerHandler({type: 'scroll', position: target});
    
            setTimeout(function() {
                verify_scroller(all, target);
                start();
            }, 500);
        });
    
        asyncTest('filter exact', function() {
            var filter_value = 23;
        
            hour.filter(filter_value);
            $('#top').trigger('cf.update');
        
            var filtered = hour.top(Infinity)
                .filter(function(x) {
                    var val = group(dimension(x));
                    return val === filter_value;
                });
    
            setTimeout(function() {
                verify_scroller(filtered);
                start();
            }, 500);
        });
        
        asyncTest('filter reset', function() {
            hour.filter();
            $('#top').trigger('cf.update');
         
            setTimeout(function() {
                verify_scroller(all);
                start();
            }, 500);
        });
    
        function verify_scroller(filter, target) {
            expect(3);
    
            var boxes = d3.selectAll("scrollerbox"),
                box = d3.select("scrollerbox").node(),
                bw = box.offsetWidth,
                container = d3.select('#top').node(),
                cw = container.offsetWidth,
                visible = boxes[0].filter(function(x) { return d3.select(x).style("display") !== 'none'; }),
                markidx = target || filter.length,
                left = -(markidx * bw) + Math.floor(cw/2) + Math.floor(bw / 2) + container.offsetLeft,
                sleft = d3.select('.scrollerx').node().offsetLeft;
    
            // box number should match the number of data records
            strictEqual(boxes[0].length, mydata.length,
                "box number " + boxes[0].length + " == " + mydata.length);
    
            // number of visible boxes should be equal to the number of filtered
            // data records
            strictEqual(visible.length, filter.length,
                "visible box number " + visible.length + " == " + filter.length);
    
            // current index should be at specified location
            // FIXME: bug! find out the root cause of discrepency and revise this
            // calculation
            ok (Math.abs(sleft - left) < 10,
                "marker index " + sleft + " - " + left + " < +/- 10");
        }
    
        function snapshot(title) {
            var container = $('<div>');
            var clone = $('#top .CFScrollerX.chart').clone();
            container.text(title);
            container.append(clone);
            snapholder.append(container);
        }
    }
});
