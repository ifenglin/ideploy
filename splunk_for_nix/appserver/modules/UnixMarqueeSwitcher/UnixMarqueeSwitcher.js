Splunk.Module.UnixMarqueeSwitcher = $.klass(Splunk.Module, { 

    initialize: function($super, container) {
        $super(container);
        this.index = -1;
        this.interval = parseInt(this.getParam('interval'))*1000;
        this.set_interval = null;
    },

    initMarquee: function() {
        if (this.set_interval === null) {
            this.shiftMarquee();
            this.set_interval = setInterval(this.shiftMarquee.bind(this), this.interval);
        }
    },

    onLoadStatusChange: function($super, statusInt) {
        $super(statusInt);
        if (statusInt > Splunk.util.moduleLoadStates.WAITING_FOR_HIERARCHY) {
            this.hideDescendants(this.moduleId);
            this.initMarquee();
        }
    },

    shiftMarquee: function() {
        if (this.index >= 0) {
            this._children[this.index].hide(this.moduleId);
        }
        
        if (this.index === this._children.length-1) {
            this.index = 0; 
        } else {
            this.index++;
        }
        
        this._children[this.index].show(this.moduleId);
    }


});

