How to run tests

Prerequisit:
    nodejs/npm

Run test:
    1. export MRSPARKLE=/path/to/splunk/search_sparkle (either perforce source
       or a splunk installation instance)
       e.g. /opt/splunk/share/splunk/search_mrsparkle
       e.g. /perforce/splunk/current/web/search_mrsparkle
    2. (optional) export APPDIR=/path/to/folder/containing/apps
       e.g. /opt/splunk/etc/
       e.g. /perforce/splunk/solutions/unix/mainline/
    3. 'make test'

Clean up:
    $ make clean
