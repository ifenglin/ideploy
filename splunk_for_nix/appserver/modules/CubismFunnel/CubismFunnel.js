Splunk.Module.CubismFunnel = $.klass(Splunk.Module, {

    initialize: function($super, container) {
        $super(container);
        this.logger = Splunk.Logger.getLogger("cubism_funnel.js");
        this.sources = this.getParam('sources').split(",");
        this.ctxid = this.getParam('contextId');
        this.hide(this.HIDDEN_MODULE_KEY);
    },

    getModifiedContext: function() {
        var context = this.getContext();

        if (this.results) {
            context.set('namespace', this.ctxid);
            context.set(this.ctxid, this.results);
            console.log("push context to children");
        }

        return context;
    },

    onContextChange: function($super) {
        var context = this.getContext(),
            sources = this.sources,
            ctxid = this.ctxid,
            self = this,
            missing = sources.some(function(d) {
                if (!context.has(d)) {
                    //console.debug("source [" + d + "] missing", d in context);

                    return true;
                }

                return false;
            });

        if (missing)
            return;

        if (this.results) {
            return;
        }

        cubismize();

        function cubismize() {
            var time_extent = [],
                binstats = {},
                metrics = [],
                evts = {},
                size = 0;

            // format data
            // FIXME: parameterize the use of '_time', 'time', and 'sourcetype'
            sources.forEach(function(d) {
                var evt = context.get(d),
                    fields = Object.keys(evt[0]).filter(function(d) { return (d != '_time') && (d != 'sourcetype'); });

                evt.reverse();
                evts[d] = evt;
                console.log("event " + d, evt);

                // coerce data to Numbers
                evt.forEach(function(d) {
                    fields.forEach(function(f) {
                        d[f] = parseInt(d[f], 10);
                    });
                });

                fields.splice(fields.indexOf('time'), 1);
                fields.forEach(function(f) { if (!(f in binstats)) binstats[f] = []; });

                var extent = d3.extent(evt, function(d) { return d.time; });

                time_extent.push(extent[0]);
                time_extent.push(extent[1]);
            });
            
            time_extent = d3.extent(time_extent);
            size = time_extent[1] - time_extent[0];

            // interpolate data
            //var delta = Math.floor((time_extent[1] - time_extent[0]) / size),
            var delta = 1,
                ts = [];
            console.log(time_extent, size, delta);

            for (var i = 0, t = time_extent[0]; i < size; i++, t += delta) {
                ts.push(t);
            }
            console.log(ts);

            sources.forEach(function(d) {
                var evt = evts[d],
                    lptr = 0,
                    rptr = 1,
                    fields = Object.keys(evt[0]).filter(function(d) { return (d != 'time') && (d != '_time') && (d != 'sourcetype'); });

                // interpolation
                for (var i = 0; i < size; i++) {
                    // before earliest or after latest data
                    if (evt[lptr].time > ts[i] || rptr == evt.length) {
                        fields.forEach(function(f) { 
                            binstats[f].push(evt[lptr][f]); });
                        continue;
                    }
                    
                    if (evt[rptr].time < ts[i]) {
                        while (rptr < evt.length && evt[rptr].time < ts[i])
                            rptr++;
                        lptr = rptr - 1;
                    }

                    if (rptr == evt.length) // reach the end of data
                        fields.forEach(function(f) { binstats[f].push(0); });
                    else {
                        var t = (ts[i] - evt[lptr].time) / (evt[rptr].time - evt[lptr].time);

                        fields.forEach(function(f) { if (binstats[f] === undefined) console.log(f); binstats[f].push(t * evt[lptr][f] + (1 - t) * evt[rptr][f]); });
                    }
                }

                fields.forEach(function(d) { console.log(d, binstats[d]); });
            });

            // store the results
            self.results = {values: binstats,
                    size: size,
                    step: delta * 1000,
                    serverDelay: ts[ts.length-1] * 1000};
        }
    }
});
