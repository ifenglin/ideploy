Splunk.Module.UnixTokenReplacer = $.klass(Splunk.Module, {

    initialize: function($super, container) {
        $super(container);
        this.hide('HIDDEN MODULE KEY');
    },



    getModifiedContext: function($super) {
        var context = this.getContext(),
            form = context.get('form'),
            search = context.get('search'),
            new_search = search.toString(),
            search_tokens = Splunk.util.discoverReplacementTokens(new_search),
            token,
            metric = form["metric"],
            cmd = form["cmd"],
            host = form["host"],
            metric_field=[],
            metric_cmd=[],
            hostString='',
            searchCmd="";
         
        if (search_tokens.indexOf("metric") > -1) {
        
        	search_tokens.push("host");
        }    
        
        console.log('replacer', search_tokens, form);
        
        
        if (metric == null || cmd == null) {
        
        	console.log("empy metric.");
			return context;	        
        }
        
         
         
        for (var i =0; i < metric.length && metric[i] !== null ; i++) {
        
            var tmpCmd='';
        
        	for (var j=0; j< cmd.length; j++) {
		
		        var index= cmd[j].indexOf(metric[i]);
		         
				//greater than 10 just to filter out those command not in the beginning
				if (index ==0) {
				
					tmpCmd = cmd[j].split("&")[1];	
				
					break;	
				}
			}

        	if (tmpCmd!= undefined && tmpCmd.length > 0) {
        		metric_cmd.push(tmpCmd);
        	} else {
        		metric_field.push(metric[i]);
        	}
        }
        
        for (var i=0; i < metric_cmd.length; i++) {
        
        	if (searchCmd.length ==0 && metric_cmd[i] != undefined)  {
        		searchCmd = searchCmd + metric_cmd[i]; 
        	} else {
        		
        		searchCmd = searchCmd + " | appendcols [ search " + metric_cmd[i] + " ]" ; 
        		
        	}
        }
        
        for (var i=0; i < metric_field.length; i++) {
        
        	if (i ==0)  {
        		
        		searchCmd = searchCmd + " | fields *" + metric_field[i] +  "* "; 
    
        	} else {
        	
        		searchCmd += " *" +  metric_field[i] + "* ";
        	}
        	
        }
        
         
        
        if (host != null) {
        	for (var i=0; i < host.length; i++) {
        	
        	  if(host[i] != null) {
        		if (i==0) {
        			hostString = 'host=' + host[i] + ' ';        		
        		}	else {
					hostString = hostString + " OR host=" + host[i];        	
        		}
        	  }
        	}
        }
        
         
 	    token = new RegExp("\\$host\\$");
 	    
 	    if (hostString.length > 0 ) {
 	    	searchCmd = Splunk.util.replaceTokens(searchCmd, token, hostString);  
 	    	
 	    	// handles multiple token in the search command
       	   while( searchCmd.indexOf("$host$") > 0) {
            	searchCmd = Splunk.util.replaceTokens(searchCmd, token, hostString);   
        	}   
        }
        
        console.log('searchCmd: ', searchCmd);
        
        search.setBaseSearch(searchCmd);
        context.set('search', search);
        
        /**
        if (form != null && form != undefined && search_tokens.length > 0) {
            for (i=0; i < search_tokens.length; i++) {
            
            	if (search_tokens[i] == "metric" && form["metric"] !== undefined) {
            	
                	token = new RegExp("\\$" + search_tokens[i] + "\\$");
                	var tmp = form[search_tokens[i]],
                        new_str = "",
                        j;
                          
                    if (tmp === null) { 
                        new_str = ""; 
                    } else if ($.isArray(tmp) === true) {
                        for (j=0; j < tmp.length; j++) {
                        	
                        	if(tmp[j]!==undefined && j==0) {                        		
                        	  new_str =tmp[0]; 	
                        	}
                        	else if(tmp[j]!==undefined) { 
                        	  new_str = new_str + " | appendcols [search " + tmp[j] + " ]"; 
                        	  
                        	} 	
                        }                       
                    } else {
                        new_str +=  tmp;
                    }
                    
                    new_search = Splunk.util.replaceTokens(new_search, token, new_str);  
				}	       
                else if (form[search_tokens[i]] !== undefined) {
                
                    var tokenString ="$" + search_tokens[i] + "$";
                
                    token = new RegExp("\\$" + search_tokens[i] + "\\$");
                    var tmp = form[search_tokens[i]],
                        new_str = '( ' + search_tokens[i] + '=',
                        j;
                    if (tmp === null) {
                        new_str = ''; 
                    } else if ($.isArray(tmp) === true) {
                        for (j=0; j < tmp.length; j++) {
                            if (tmp[j] !== undefined) {
                                new_str += '"' + tmp[j] + '"';
                                if (tmp.length !== (j + 1)) {
                                    new_str += ' OR ' + search_tokens[i] + '=';
                                }
                            }
                        } 
                        new_str += ' )';
                    } else {
                        new_str +=  '"' + tmp + '" )';
                    }
                    new_search = Splunk.util.replaceTokens(new_search, token, new_str);  
             
                    // handles multiple token in the search command
                    while( new_search.indexOf(tokenString) !== -1) {
                     	new_search = Splunk.util.replaceTokens(new_search, token, new_str); 
                    }      
                }
            } 
            
            new_search = new_search.replace(/'/g,'"');
            search.setBaseSearch(new_search);
            context.set('search', search);
        }
        **/
        
        return context;
    }

});

