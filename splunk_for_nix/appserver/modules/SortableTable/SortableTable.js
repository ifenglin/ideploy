Splunk.Module.SortableTable = $.klass(Splunk.Module, {
    initialize: function ($super, container) {


        $super(container);

        this.div_id = '#' + this.moduleId + '_sortabletable';
        this.svg_id = this.moduleId + '_svg';

    },

    displayTable: function (data) {

        d3.selectAll(".g-column").remove();
        d3.selectAll("#g-chart .g-row").remove(); 
		d3.selectAll("#g-chart .g-compare").remove(); 
		
        var districts = d3.csv.parse(data);

        var chicago = districts[0];

		var headers = getMetricArray(districts); 
		
		 for (x in chicago) {
		
			if (!isNaN(chicago[x]) && chicago[x] !== undefined && chicago[x] !== null && chicago[x] < 100) {
				chicago[x]=75;
			} 
			else if (!isNaN(chicago[x]))  {
				chicago[x]=chicago[x] * 1.2;
			}
			else {
				chicago[x] = "threshold"; 
			}
		} 
		
		console.log("chicago: " + chicago);
		    
		function getMetricArray(datas) {

            var row = datas[0];
            var metrics = [];
		
			metrics.push("_time");

            for (var name in row) {
               if (name  && name.charAt(0) != "_" && name.charAt(0) != "t"  && name.charAt(0) != "#")  {
                    metrics.push(name);
                }
            }
            
            for (var i=0; i < metrics.length; i++) {  
           	
           	 	d3.select(".g-body")
					.append("div")
					.attr("class" ,"g-column").append("div")
					.attr("class" , "g-subtitle").append("div")
					.text(metrics[i]); 
			}
			
			var expandWidth= metrics.length *170;
			var expandHeight = districts.length*30;
			
			d3.select(".SortableTableContainer").style("min-width",expandWidth+"px");
			//d3.select(".SortableTableContainer").style("min-height",expandHeight+"px");
			
            d3.select("#g-chart").style("min-width",expandWidth+"px");
            //d3.select("#g-chart").style("min-height",expandHeight+"px");
            
            return metrics;
        }	

        districts.sort(function (a, b) {
            return (b === chicago) - (a === chicago) || d3.ascending(a.name, b.name);
        });

        districts.forEach(function (district, i) {
            district.index = i;
        });

        var formatDollars = d3.format(".3s"),
            formatMinutes = d3.format("02d"),
            formatHours = function (d) {
                return isNaN(d) ? "N.A." : Math.floor(d) + ":" + formatMinutes(Math.round((d % 1) * 60));
            },
            formatCurrency = function (d) {
                return isNaN(d) ? "N.A." : formatDollars(+d);
            },
            formatCategorical = function (d) {
                return d === true ? "●" : d === false ? "○" : "N.A.";
            },
            formatDefault = String;

        var salaryScale = d3.scale.linear().domain([0, d3.max(districts, function (d) {
            //return d["@cpu%@af-yoda.sv.splunk.com"];
            return 100;
        })]).range([0, 156]),
            timeScale = d3.scale.linear().domain([0, 9]).range([0, 156]);

		var dimensions = [];
		
		for( var i =0;i < headers.length; i++) {
		
			var temp = {};
			
			if(headers[i] == "_time") { 
				temp={name:"_time",format: formatDefault};
			}
			else {
				temp = {name: headers[i],
          			  format: formatCurrency,
           			  scale: salaryScale
           		};
			}
			
			dimensions.push(temp);
		}

		/**
        var dimensions = [{
            name: "time",
            format: formatDefault
        }, {
            name: "@cpu%@af-yoda.sv.splunk.com",
            format: formatCurrency,
            scale: salaryScale
        }, {
            name: "@cpu%@af-yolo.sv.splunk.com",
            format: formatCurrency,
            scale: salaryScale
        }, {
            name: "@cpu%@appletekiMacBook-Pro.local",
            format: formatCurrency,
            scale: salaryScale
        }, {
            name: "@memUsed%@af-yoda.sv.splunk.com",
            format: formatCurrency,
            scale: salaryScale
        }, {
            name: "@memUsed%@af-yolo.sv.splunk.com",
            format: formatCurrency,
            scale: salaryScale
        }, {
            name: "@memUsed%@appletekiMacBook-Pro.local",
            format: formatCurrency,
            scale: salaryScale
        } ]; 
        
        **/

        dimensions.forEach(function (dimension, i) {
            dimension.index = i;
        });

		//alert("dimentsions:" + dimensions);
		

        var row = d3.select("#g-chart").selectAll(".g-row")
            .data(districts)
            .enter().append("g")
            .attr("class", "g-row")
            .classed("g-special", function (district) {
            return district === chicago;
        })
            .attr("transform", function (district, i) {
            return "translate(0," + i * 21 + ")";
        });

        row.append("rect")
            .attr("class", "g-background")
            .attr("width", "200%")
            .attr("height", 20);

        row.each(function (district) {
            var cell = d3.select(this).selectAll(".g-cell")
                .data(dimensions)
                .enter().append("g")
                .attr("class", "g-cell")
                .classed("g-quantitative", function (dimension) {
                return dimension.scale;
            })
                .classed("g-categorical", function (dimension) {
                return dimension.categorical;
            })
                .attr("transform", function (dimension, i) {
                return "translate(" + i * 160 + ")";
            });

			//alert("cell: " + cell);	

            var quantitativeCell = cell.filter(function (dimension) {
            
            /**
            	alert("dimension.scale: " +dimension.scale + " district[dimension.name]: " + district[dimension.name] + 
     " result: " +  (dimension.scale && !isNaN(district[dimension.name])) ); **/
            
                return dimension.scale && !isNaN(district[dimension.name]);
            });
            
            //alert("quantitativeCell: " + quantitativeCell);
            

            quantitativeCell.append("rect")
                .attr("class", "g-bar")
                .attr("width", function (dimension) {
                
                /**
                alert("chicago[dimension.name]: " +chicago[dimension.name] + " district[dimension.name]: " +district[dimension.name] 
                 + " reuslt:" +dimension.scale(Math.min(chicago[dimension.name], district[dimension.name]))); **/
                	
                return dimension.scale(Math.min(chicago[dimension.name], district[dimension.name]));
            })
                .attr("height", 20);
                
            
            //alert("after append rect");    

            quantitativeCell.filter(function (dimension) {
                return +district[dimension.name] > +chicago[dimension.name];
            }).append("rect")
                .attr("class", "g-bar-cap")
                .attr("x", function (dimension) {
                return dimension.scale(chicago[dimension.name]);
            })
                .attr("width", function (dimension) {
                return dimension.scale(district[dimension.name] - chicago[dimension.name]);
            })
                .attr("height", 20);

            cell.append("text")
                .attr("class", "g-value")
                .attr("x", function (dimension) {
                return dimension.scale ? 3 : 0;
            })
                .attr("y", function (dimension) {
                return dimension.categorical ? 9 : 10;
            })
                .attr("dy", ".35em")
                .classed("g-na", function (dimension) {
                return district[dimension.name] === undefined;
            })
                .classed("g-yes", function (dimension) {
                return district[dimension.name] === true;
            })
                .classed("g-no", function (dimension) {
                return district[dimension.name] === false;
            })
                .text(function (dimension) {
                return dimension.format(district[dimension.name]);
            })
                .attr("clip-path", function (dimension) {
                return (dimension.clipped = this.getComputedTextLength() > 140) ? "url(#g-clip-cell)" : null;
            });

            cell.filter(function (dimension) {
                return dimension.clipped;
            }).append("rect")
                .style("fill", "url(#g-clip-gradient)")
                .attr("x", 136)
                .attr("width", 20)
                .attr("height", 20);
        });

        d3.selectAll(".g-special .g-categorical").append("text")
            .attr("class", "g-value g-label")
            .attr("x", 15)
            .attr("y", 10)
            .attr("dy", ".35em")
            .text(function (dimension) {
            return chicago[dimension.name] ? "Yes" : "No";
        });

        d3.select("#g-chart").selectAll(".g-compare")
            .data(dimensions.filter(function (dimension) {
            return dimension.scale;
        }))
            .enter().append("line")
            .attr("class", "g-compare")
            .attr("transform", function (dimension) {
            return "translate(" + (dimension.scale(chicago[dimension.name]) + dimension.index * 160) + ",0)";
        })
            .attr("y2", "100%");

        d3.selectAll(".g-body .g-subtitle")
            .data(dimensions)
            .on("click", sort);

        function sort(dimension) {
            var dimensionName = dimension.name,
                descending = d3.select(this).classed("g-ascending");

            d3.selectAll(".g-descending").classed("g-descending", false);
            d3.selectAll(".g-ascending").classed("g-ascending", false);

            if (descending) {
                d3.select(this).classed("g-descending", true);
                var orderQuantitative = function (a, b) {
                    return ((b === chicago) - (a === chicago)) || (isNaN(a[dimensionName]) - isNaN(b[dimensionName])) || (a[dimensionName] - b[dimensionName]) || (a.index - b.index);
                };

                var orderName = function (a, b) {
                    return ((b === chicago) - (a === chicago)) || b.name.localeCompare(a.name);
                };
            } else {
                d3.select(this).classed("g-ascending", true);

                var orderQuantitative = function (a, b) {
                    return ((b === chicago) - (a === chicago)) || (isNaN(a[dimensionName]) - isNaN(b[dimensionName])) || (b[dimensionName] - a[dimensionName]) || (a.index - b.index);
                };

                var orderName = function (a, b) {
                    return ((b === chicago) - (a === chicago)) || a.name.localeCompare(b.name);
                };
            }

            d3.selectAll(".g-row")
                .sort(dimensionName === "name" ? orderName : orderQuantitative)
                .each(function (district, i) {
                district.index = i;
            })
                .transition()
                .delay(function (district, i) {
                return (i - 1) * 10;
            })
                .duration(750)
                .attr("transform", function (district, i) {
                return "translate(0," + i * 21 + ")";
            });
        }



    },

    onContextChange: function () {

        var that = this,
            context = this.getContext(),
            dataset = context.get('dataset');
 
        //alert("on context table : " + dataset);

        if (dataset != undefined) {
            this.displayTable(dataset.results);
        }
    }

});