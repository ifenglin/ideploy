define(function() {
    var appname = 'splunk_for_nix',
        paths = window.location.pathname.split('/'),
        appidx = paths.indexOf(appname);

    var splunk_home = appidx !== -1 ? paths.slice(0, appidx - 2).join('/') + '/' : '/',
        mrsparkle = splunk_home + "share/splunk/search_mrsparkle/",
        mrsparkle_core = mrsparkle + "exposed/js/",
        mrsparkle_module = mrsparkle + "modules/",
        app_path = splunk_home + "etc/apps/splunk_for_nix/",
        appserver_path = app_path + "appserver/",
        module_path = appserver_path + "modules/";

    var corejs = [
            "core/contrib/jquery-1.6.2.min.js",
            "core/contrib/lowpro_for_jquery.js",
            "core/contrib/script.js",
            "core/splunk.js",
            "core/util.js",
            "core/logger.js",
            "core/context.js",
            "core/i18n.js",
            "core/time_range.js",
            "core/job.js",
            "core/search.js",
            "core_module/AbstractModule.js"
        ].map(function(d) { return "js!" + d + '!order'; });

    var config = {
        baseUrl: '../',
        paths: {
            curl: 'lib/curl/',
            splunk: splunk_home,
            app: splunk_home + '/etc/apps/',
            core: mrsparkle_core,
            core_module: mrsparkle_module
            //cssx: 'cssx/src/cssx',
        }
    };

    return {
        splunk_home: splunk_home,
        mrsparkle: mrsparkle,
        mrsparkle_core: mrsparkle_core,
        mrsparkle_module: mrsparkle_module,
        app_path: app_path,
        appserver_path: appserver_path,
        module_path: module_path,
        config: config,
        core: corejs
    };
});
