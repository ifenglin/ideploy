Splunk.Module.UnixAlertsSeverity = $.klass(Splunk.Module, { 

    initialize: function ($super, container) {
        $super(container);

        this.$buttons = $('a.severity', this.container);

        this.namespace = this.getParam('namespace') || 'form';
        this.severity = null;
        this.token = this.getParam('token');

        this.initBindings();
    },

    getToken: function() {
        return this.token;
    },
 
    updateSeverity: function(values) {
        var severities = [];

        if (values !== null) {
            for (var i=0; i<values.length; i++) {
                severities.push(values[i]);
            }
        }
 
        if (severities.length === 0) {
            severities = null;
        }

        this.severity = severities;
    },

    onContextChange: function() {
        var context = this.getContext(),
            value = context.get(this.namespace + '.' + this.token) || null,
            values, i, v;

        if (value !== null && value !== undefined) {
            values = value.split(',');
            for (i=0; i < values.length; i++) {
                v = parseInt(values[i]);
                this.$buttons.eq(v).trigger('click');
            }
        }

    },

    getModifiedContext: function() {
        var context = this.getContext();

        context.set(this.namespace + '.' + this.token, this.getSeverity());

        return context;
    },

    initBindings: function($super) {
        var that = this;

        this.$buttons.click(function(e) {
            var $elm = $(e.target),
                id = $elm.attr('id'),
                severities = this.getSeverity(),
                all_idx;

            e.preventDefault();

            if (id === 'all') {
                $elm.siblings().removeClass('selected');
                $elm.addClass('selected');
                severities = null;
            } else if ($elm.hasClass('selected') === true) {
                $elm.removeClass('selected'); 
                if (severities !== null) {
                    severities.splice(severities.indexOf(id),1);
                    if (severities.length === 0) {
                        severities = null;
                    }
                }
            } else {
                $elm.siblings('#all').removeClass('selected');
                $elm.addClass('selected');
                if (severities !== null) {
                    all_idx = severities.indexOf('all');
                    if (all_idx != -1) {
                        severities.splice(severities.indexOf('all'),1);
                    }
                    severities.push(id);
                } else {
                    severities = [id];
                }
            }
            this.updateSeverity(severities);
            this.pushContextToChildren();
          
        }.bind(this));
    },

    getSeverity: function() {
        return this.severity;
    }

});

