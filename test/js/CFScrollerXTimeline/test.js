curl(['../common/envs', 'domReady!'], function(envs) {
    var config = envs.config,
        core = envs.core,
        module_name = 'CFScrollerXTimeline',
        deps = [
            "js!appserver/static/d3.v2.min.js",
            "js!appserver/static/crossfilter.union.min.js",
            "js!appserver/static/custom.js",
            "js!modules/CFScrollerXTimeline/CFScrollerXTimeline.js",
            "js!modules/CFScrollerXTimeline/timeline.js",
            "js!my/data.js",
            "css!appserver/modules/CFScrollerXTimeline/CFScrollerXTimeline.css"
        ];

    var context, dataset;

    config.paths.my = 'CFScrollerXTimeline';
    config.paths.appserver = config.paths.splunk + '/etc/apps/splunk_for_nix/appserver/';
    config.paths.modules = config.paths.appserver + '/modules/';

    curl(config, core)
        .next(deps)
        .then(
            function() {
                setup();
                do_test();
            },
            function(ex) {
                console.log(ex);
            }
        );

    function setup() {
        // mock $script, include them directly in the above
        var $script = function() {};
        
        // inject our own context directly
        context = context_from_object(my_context);
        
        dataset = crossfilter(mydata);
        
        mydata.forEach(function(d) { d.date = new Date(d.date); })
        
        context.set('dataset', dataset);
        context.set('controller', 'controller');
        context.set('namespace', 'unixcf');
        context.set('unixcf', {time_of_day: unixcf.time_of_day,
          date: unixcf.date,
          date_name: unixcf.date_name});
        
        Splunk.Module.loadParams = {'top': {id: 'timeline', width: "600", height: "30", config: 'date', barwidth: "13"}};
        
        Splunk.Module.CFScrollerXTimeline = $.klass(Splunk.Module.CFScrollerXTimeline, {
          getContext: function() {
            return context;
          }
        });
        
        var target = $('#top');
        $('#top').attach(Splunk.Module.CFScrollerXTimeline, document.getElementById('top'));
        
        // test CFScrollerXTimeline.onContextChange
        target.triggerHandler('ContextChange');
    }
    
    // TODO: verify actual data is correct and stack location matches too
    // TODO: restructure tests
    function do_test() {
        var dimension = unixcf.time_of_day.dimension,
            group = unixcf.time_of_day.group,
            hour = dataset.dimension(dimension),
            hours = hour.group(group),
            all = hour.top(Infinity);
            snapholder = $('#snapshots_holder');
    
        //QUnit.config.reorder = false;
    
        asyncTest('default', function() {
            setTimeout(function() {
                snapshot('default');
                verify_stack(all);
                start();
            }, 1000);
        });
    
        test('click on data bar', function() {
            var number = 0,
                wall = d3.selectAll('#top .wall')[0][number],
                somebar = d3.select(wall),
                bx = somebar.attr('x'),
                by = somebar.attr('y'),
                bw = somebar.attr('width'),
                bh = somebar.attr('height'),
                node = somebar.node(),
                marker = d3.select('#top image.pointer'),
                scrolled = false;
    
            var evt = document.createEvent('MouseEvents');
    
            expect(2);
    
            // NOTE: remove after communication between scrollerx and 
            // scrollerx_timeline modules is fixed
            $('.scrollerx').bind('scroll', function(evt) { scrolled = true;});
    
            evt.initMouseEvent("click", true, true, window,
                0, bx, by, bw/2, by/2, false, false, false, false, 0, null);
    
            node.dispatchEvent(evt);
    
            snapshot('click on data bar');
            strictEqual(bx, marker.attr('x').slice(0, -2), 'Marker aligned');
            strictEqual(scrolled, true, 'Scroller notified');
        });
    
        test('click on empty bar', function() {
            var number = 13,
                wall = d3.selectAll('#top .wall')[0][number],
                somebar = d3.select(wall),
                bx = somebar.attr('x'),
                by = somebar.attr('y'),
                bw = somebar.attr('width'),
                bh = somebar.attr('height'),
                node = somebar.node(),
                marker = d3.select('#top image.pointer'),
                init = marker.attr('x'),
                scrolled = false;
    
            var evt = document.createEvent('MouseEvents');
    
            expect(2);
    
            // NOTE: remove after communication between scrollerx and 
            // scrollerx_timeline modules is fixed
            $('.scrollerx').bind('scroll', function(evt) { scrolled = true;});
    
            evt.initMouseEvent("click", true, true, window,
                0, bx, by, bw/2, by/2, false, false, false, false, 0, null);
    
            node.dispatchEvent(evt);
    
            snapshot('click on empty bar');
            strictEqual(marker.attr('x'), init, 'Marker unchanged');
            strictEqual(scrolled, false, 'Scroll event not fired');
        });
    
        test('scroller move', function() {
            expect(1);
    
            $('#top div.CFScrollerXTimeline.chart').triggerHandler('scroll', 0);
    
            var marker = d3.select('#top image.pointer');
    
            strictEqual(marker.attr('x'), "0",
                'Marker updated after scroller movement');
        });
    
        module('crossfilter');
    
        asyncTest ('filter exact', function() {
            var filter_value = 15;
                
            var filtered = all.filter(function(x) {
                    return dimension(x) === filter_value;
                });
    
            hour.filter(filter_value);
            $('#top').trigger('cf.update');
        
            setTimeout(function() {
                snapshot('filter exact');
                verify_stack(filtered);
                start();
            }, 1000);
        });
    
        asyncTest ('filter range', function() {
            var filter_value = [15, 17];
                
            var filtered = all.filter(function(x) {
                    var val = dimension(x);
                    return val < filter_value[1] && val >= filter_value[0];
                });
    
            hour.filter(filter_value);
            $('#top').trigger('cf.update');
        
            setTimeout(function() {
                snapshot('filter range');
                verify_stack(filtered);
                start();
            }, 1000);
        });
        
        asyncTest('filter reset', function() {
            hour.filter();
            $('#top').trigger('cf.update');
         
            setTimeout(function() {
                snapshot('filter reset');
                verify_stack(all);
                start();
            }, 1000);
        });
    
        function verify_stack(filter) {
            expect(1);
    
            var bars = d3.select("div.CFScrollerXTimeline.chart")
              .selectAll("g.bar")
              .data();
    
            var histo = d3.layout.histogram()
                    .value(unixcf.date.dimension)
                    .bins(function(e) {
                        return d3.time.day.range(e[0], 
                            d3.time.day.offset(e[1], 2));
                    })(filter),
                sum = d3.sum(histo, function(x) { return x.y ? 1 : 0; });
    
            strictEqual(bars.length, sum, "stack number " + bars.length + " == " + sum);
        }
    
        function snapshot(title) {
            var container = $('<div>');
            var clone = $('#top .CFScrollerXTimeline.chart').clone();
            container.text(title);
            container.append(clone);
            snapholder.append(container);
        }
    }
});
