Splunk.Module.Unix_Crossfilter = $.klass(Splunk.Module.DispatchingModule, {

    initialize: function($super, container) {
        
        $super(container);
        var that = this;

        this.logger = Splunk.Logger.getLogger("unix_crossfilter");
        this.ctxid = this.getParam('contextId') || 'dataset';

        $('#' + this.moduleId)
            .bind('update', notifykids);

        function notifykids(e) {
            var type = e.type;
            $('[data-' + type + ']').each(function() {
                $(this).triggerHandler("cf." + type);
            });
            e.stopPropagation();
        }
    },

    getModifiedContext: function() {
        var context = this.getContext(),
            ctxid = this.ctxid;

        if (this[ctxid]) {
            context.set(ctxid, this[ctxid]);
            context.set('controller', this.moduleId);
        }

        return context;
    },

    onContextChange: function($super) {
        var context = this.getContext();
    },

    onJobDone: function() {
        this.getResults();
    },

    getResultParams: function($super) {
        var params = $super();
        var context = this.getContext();
        var search = context.get("search");
        var sid = search.job.getSearchId();

        if (!sid) this.logger.error(this.moduleType, "Assertion Failed.");

        params.sid = sid;
        return params;
    },

    renderResults: function(response) {
        if (response !== undefined && response !== null && response.results) {
            var data = response.results,
                ctxid = this.ctxid;

            // A little coercion
            data.forEach(function(d, i) {
                d.date = new Date(d._time);
            });

            this[ctxid] = crossfilter(data);
            this[ctxid].data = data;
            this.pushContextToChildren();
        }
    }
});
