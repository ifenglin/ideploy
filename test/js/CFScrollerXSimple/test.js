curl(['../common/envs', 'domReady!'], function(envs) {
    var config = envs.config,
        core = envs.core,
        module_name = 'CFScrollerXSimple',
        deps = [
            "js!appserver/static/d3.v2.min.js",
            "js!appserver/static/crossfilter.union.min.js",
            "js!appserver/static/custom.js",
            "js!modules/CFScrollerXSimple/CFScrollerXSimple.js",
            "js!modules/CFScrollerXSimple/simple.js",
            "js!my/data.js",
            "css!appserver/modules/CFScrollerXSimple/CFScrollerXSimple.css"
        ];

    var context, dataset;

    config.paths.my = 'CFScrollerXSimple';
    config.paths.appserver = config.paths.splunk + '/etc/apps/splunk_for_nix/appserver/';
    config.paths.modules = config.paths.appserver + '/modules/';

    curl(config, core)
        .next(deps)
        .then(
            function() {
                setup();
                do_test();
            },
            function(ex) {
                console.log(ex);
            }
        );

    function setup() {
        var $script = function() {};
    
        var context = context_from_object(my_context),
            dataset = crossfilter(mydata),
            dimension = dataset.dimension(unixcf.scroller.dimension),
            group = dimension.group(),
            div = d3.select('#top').data(simple_data);
    
        mydata.forEach(function(d) { d.date = new Date(d.date); })
    
        context.set('dataset', dataset);
        context.set('cfscroller_data', {
          selection: div,
          dimension: dimension,
          group: group,
          level: 1
        });
        context.set('namespace', 'unixcf');
        context.set('unixcf', {time_of_day: unixcf.time_of_day,
          date: unixcf.date,
          date_name: unixcf.date_name,
          scroller: unixcf.scroller});
    
        Splunk.Module.loadParams = {'top': {config: 'scroller'}};
    
        Splunk.Module.CFScrollerXSimple = $.klass(Splunk.Module.CFScrollerXSimple, {
          getContext: function() {
            return context;
          }
        });
    
        var target = $('#top'),
            ksimple = $('#top').attachAndReturn(Splunk.Module.CFScrollerXSimple, document.getElementById('top'))[0];
    
        var searches = ['| rest /services/search/jobs search=sid=' + simple_data[0].sid];
    
        searches.forEach(function(s) { ksimple.tracker[s] = true; });
    
        // test CFScrollerXSimple.onContextChange
        target.triggerHandler('ContextChange');
    }
    
    // TODO: verify actual data is correct and stack location matches too
    // TODO: restructure tests
    function do_test() {
        var snapholder = $('#snapshots_holder');
    
        //QUnit.config.reorder = false;
    
        test('default', function() {
            ok ( true, 'Pass');
        });
    
        function snapshot(title) {
            var container = $('<div>');
            var clone = $('#top .CFScrollerXSimple.chart').clone();
            container.text(title);
            container.append(clone);
            snapholder.append(container);
        }
    }
});
