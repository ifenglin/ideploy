from urllib import urlencode
import re
import Cookie

import httplib2
from lxml import html

# known issue:
# sometimes request will raise httplib.BadStatusLine exception

class SplunkWeb(object):
    """Usage:

    >>> splunkweb = SplunkWeb()
    >>> response, content = splunkweb.request('/en-US/app/launcher/home', 'GET', {'foo','bar'}, {'Content-Type': 'charset=UTF-8'})
    """

    def __init__(self, username='admin', password='changeme',
                       hostpath='http://localhost:8000'):
        self.hostpath = hostpath
        self.http = httplib2.Http(disable_ssl_certificate_validation=True)
        self.http.follow_redirects = 0
        self.session = None
        self._login(username, password)
        self.formkey = self._get_formkey()

    def _login(self, username, password):
        url = self.hostpath + '/en-US/account/login'
        response, content = self.http.request(url, 'GET')
        assert response.status == 200, response

        cval = re.findall(r'cval=(\d+)', response['set-cookie'])[0]
        headers = {
            'Cookie':"cval=%s" % cval,
            'Content-type':'application/x-www-form-urlencoded'
        }
        body = urlencode({
            'username':username,
            'password':password,
            'cval':cval
        })
        response, content = self.http.request(url, 'POST', body, headers)

        if response.status == 200:
            # check if it's first time login
            title = html.fromstring(content).xpath('//head/title')[0].text
            assert 'change password' in title
        else:
            assert response.status == 303

        cookie = response['set-cookie']
        self.session = re.findall(r'session_id_8000=(\w+)', cookie)[0]

    def _get_formkey(self):
        formkey = None
        response, content = self.request('/en-US/app/launcher/home')
        assert response.status == 200
        try:
            formkey = re.findall(r'window.\$C\s*=\s*{.*"FORM_KEY":\s*"(\d+)"',
                content)[0]
        except:
            ''' sometimes splunkweb return partial html, so search another pattern instead '''
            try:
                formkey = re.findall(r'<input type="hidden" name="splunk_form_key" value="(\d+)" />', content)[0]
            except:
                assert 0, 'Cannot get form key'
        assert formkey
        return formkey

    def request(self, path='/', method='GET', body={}, headers={}):
        """Return tuple of (response, content) of http request.
        method = 'GET', 'POST', ...
        body = dict or urlencoded post parameters
        headers = dict of http headers
        """

        assert self.session, 'login first'
        assert path.startswith('/')
        assert method in ('OPTIONS', 'GET', 'HEAD', 'POST', 'PUT', 'DELETE',
            'TRACE', 'CONNECT', 'PATCH')

        if isinstance(body, dict):
            body = urlencode(body)

        url = self.hostpath + path
        cookie = Cookie.SimpleCookie(headers.get('Cookie'))
        cookie['session_id_8000'] = self.session
        headers['Cookie'] = cookie.output(header='Cookie:')

        if method == 'POST':
            headers.update({
            'X-Splunk-Form-Key': self.formkey,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'X-Requested-With': 'XMLHttpRequest',
            })

        response, content = self.http.request(url, method, body, headers)
        return response, content

if __name__ == '__main__':
    import doctest
    doctest.testmod()

