"""
Meta
====
    $Id: //splunk/solutions/unix/mainline/test/demo/test_nix.py#9 $
    $DateTime: 2011/05/16 11:48:08 $
    $Author: dzakharov $
    $Change: 100166 $
"""

import py
import os
import time
import logging
import marshal
from NixUtil import NixUtil
from SplunkCLI import SplunkCLI
from SearchTestUtil import SearchTestUtil
from splunk import auth
from SplunkConf import SplunkConfManager
from SearchUtil import SearchUtil

class TestNix(object):

    logger = logging.getLogger('TestNix')
    
    def setup_class(self):
        self.logger.setLevel(logging.DEBUG)
        self.splunk_home = os.environ["SPLUNK_HOME"]
        self.logger.debug("SPLUNK_HOME:" + self.splunk_home)
        self.splunk_cli = SplunkCLI(self.splunk_home)
        nixutil = NixUtil(self.splunk_home, self.logger)
        self.package = nixutil.get_and_install_nix()
        self.search_util = SearchUtil(self.logger)
 
    def setup_method(self, method):
        self.remote_key = auth.getSessionKey(username='admin', password='changeme', hostPath='')
    
    def test_assets_macro(self):
        # run simple search
        result = self.search_util.checkQueryCount(self.remote_key, "| inputlookup su_actions.csv", 3, '', 'search')
        assert result == True

    def teardown_class(self):
        self.splunk_cli.stop()
        os.remove(self.package)
