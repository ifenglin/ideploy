from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from os import environ
import unittest, time, re

class TestES(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(180)
        self.base_url = "https://%s.sv.splunk.com:%s" % ( environ['DEPLOY_HOST'], environ['HTTP_PORT'] )
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_es(self):
        driver = self.driver
        driver.get(self.base_url + "")
        driver.find_element_by_id("username").clear()
        driver.find_element_by_id("username").send_keys("admin")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("changeme")
        driver.find_element_by_css_selector("input.splButton-primary").click()
        driver.find_element_by_id("applicationsMenuActivator").click()
        driver.find_element_by_xpath("(//a[contains(text(),'Enterprise Security')])[3]").click()
        driver.find_element_by_link_text("Incident Review").click()
        driver.find_element_by_xpath("//div[@id='TimeRangePicker_0_13_0']/a").click()
        driver.find_element_by_link_text("Last 60 minutes").click()
        driver.find_element_by_css_selector("input.searchButton").click()
        # Warning: waitForTextNotPresent may require manual changes
        for i in range(60):
            try:
                if not re.search(r"^[\s\S]*scanned events[\s\S]*$", driver.find_element_by_css_selector("BODY").text): break
            except: pass
            time.sleep(1)
        else: self.fail("time out")
        # Warning: assertTextNotPresent may require manual changes
        self.assertNotRegexpMatches(driver.find_element_by_css_selector("BODY").text, r"^[\s\S]*(?<![0-9])0 matching events[\s\S]*$")
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert.text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
