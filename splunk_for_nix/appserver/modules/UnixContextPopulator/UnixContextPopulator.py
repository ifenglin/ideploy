import csv
import json
import logging
import os
import sys
import StringIO

import cherrypy
import controllers.module as module
import splunk
import splunk.search
import splunk.util
import lib.util as util
from splunk.appserver.mrsparkle.lib import jsonresponse

logger = logging.getLogger('splunk.appserver.controllers.module.UnixContextPopulator')

class UnixContextPopulator(module.ModuleHandler):

    def generateResults(self, host_app, client_app, sid, count=10000):

        output = {} 
        job = splunk.search.JobLite(sid);
        rs = job.getResults('results', count=10000)
        
        out = StringIO.StringIO()
        writer = csv.writer(out)
        
        field_list = None
        
        if field_list is None:
            field_list = [str(field_name) for field_name in rs.results()[0]]
        else:
            field_list = field_list.split(',')
        
        writer.writerow(field_list)
        for row in rs.results()[1:-1]:
            writer.writerow([str(row.get(field)) for field in field_list]) 


        data = {}
        
        data['results'] = out.getvalue()  
        
        logger.info(data['results'])
            
        return self.render_json(data)

    def render_json(self, response_data, set_mime='text/json'):
        cherrypy.response.headers['Content-Type'] = set_mime
        if isinstance(response_data, jsonresponse.JsonResponse):
            response = response_data.toJson().replace("</", "<\\/")
        else:
            response = json.dumps(response_data).replace("</", "<\\/")
        return ' ' * 256  + '\n' + response
