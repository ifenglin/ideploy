require('d3');

_i18n_locale = {locale_name: "en_DEBUG"};
navigator = window.navigator;
CSSStyleDeclaration = window.CSSStyleDeclaration;

process.env.TZ = "America/Los_Angeles";

require("./env-assert");
require("./env-xhr");
require("./env-fragment");

jQuery = require("jQuery");
$ = jQuery;

// load window.$C
require('../splunk.core.test_utils');
window.$C['FORM_KEY'] = "4130986231804560801";

require("../splunk_for_nix.v1");
