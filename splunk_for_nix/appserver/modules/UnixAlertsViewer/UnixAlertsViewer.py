import json
import logging
import os
import re
import sys
import time

from urllib import quote 

import cherrypy

import splunk
import splunk.rest

import controllers.module as module
import splunk.appserver.mrsparkle.lib.util as util

from splunk.appserver.mrsparkle.lib import jsonresponse

from splunk.models.fired_alert import FiredAlert
from splunk.models.saved_search import SavedSearch 

dir = os.path.join(util.get_apps_dir(), __file__.split('.')[-2], 'bin')
if not dir in sys.path:
    sys.path.append(dir)
    
from unix.util.timesince import *

logger = logging.getLogger('splunk')

class UnixAlertsViewer(module.ModuleHandler):
    
    def generateResults(self, host_app, client_app, count, app_namespace, severity=None, filter=None, **args):

        output = {'alerts': []}
        count = int(count)
        search_string = []

        alerts = FiredAlert.all()
        alerts = alerts.filter_by_user(cherrypy.session['user']['name'])
        alerts = alerts.filter_by_app(app_namespace)

        if filter is not None:
            search_string.append(filter)
   
        if severity is not None:
            sev = []
            severity = severity.split(',')
            for s in severity:
                sev.append('severity=%s' % s) 
            search_string.append(' OR '.join(sev))

        logger.info('search string: %s' % ' '.join(search_string))
        alerts = alerts.search('%s' % ' '.join(search_string))

        for a in alerts[:count]:
            output['alerts'].append(
                {'time': str(a.trigger_time),
                 'job_id': a.sid,
                 'severity': a.severity,
                 'timesince': timesince(a.trigger_time),
                 'name': a.savedsearch_name})

        return self.render_json(output)

    def render_json(self, response_data, set_mime='text/json'):
        cherrypy.response.headers['Content-Type'] = set_mime
        if isinstance(response_data, jsonresponse.JsonResponse):
            response = response_data.toJson().replace("</", "<\\/")
        else:
            response = json.dumps(response_data).replace("</", "<\\/")
        return ' ' * 256  + '\n' + response

