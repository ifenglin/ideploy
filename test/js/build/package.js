require("util").puts(JSON.stringify({
  "name": "splunk_for_nix",
  "version": "1.0.0",
  "description": "modules for splunk_for_nix",
  "keywords": ["splunk", "unix", "splunk_for_nix", "module"],
  "homepage": "http://www.splunk.com/",
  "author": {"name": "Liu-Yuan Lai"},
  //"repository": {"type": "git", "url": "http://github.com/mbostock/d3.git"},
  "main": "index.js",
  "browserify" : "index-browserify.js",
  "jam": {
    "main": "splunk_for_nix.js",
    "shim": {
      "exports": "splunk_for_unix"
    }
  },
  "dependencies": {
    "jsdom": "0.2.14",
    "jQuery": "1.7.2",
    "d3": "2.10.x"
  },
  "devDependencies": {
    "uglify-js": "1.3.3",
    "vows": "0.6.x"
  },
  "scripts": {"test": "./node_modules/vows/bin/vows"}
}, null, 2));
