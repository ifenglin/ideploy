#!/bin/bash
# I-Feng Lin, May 28th 2013
### You may modify the variables for your use. ###

# remote splunk settings
SPLUNK_HOME="/home/ilin/splunk-upgrade"
WEB_PORT="8006" #Alternatively, change it with options in command.
SPLUNKD_PORT="8095"
USER=root

# Below are for the script to fecth Splunk install files
# If you enable option -f to upload a install file, the variables are not used.
# releases server address
RELEASES_URL="http://releases.sv.splunk.com"

### Do not modify the variables below here unless you know what they are. ###

LOGFILE_PREFIX=$PWD/logs/upgrade

SSH="ssh -o StrictHostKeyChecking=no"
SCP="scp -pr -o StrictHostKeyChecking=no"

# a log wrapper
run() {
    LOGFILE=$1
    LINE=($@)
    COMMAND=${LINE[@]:1}
    echo "### CMD: \"$COMMAND\", PWD: $PWD" >> $LOGFILE
    #if [ "$2" != "python" ] ; then
    #    $COMMAND >> $LOGFILE 2>&1
    #else
    #    $COMMAND
    #fi
    $COMMAND
}

update_idx_hosts() {
    SPLUNK_CMD="$SPLUNK_HOME/bin/splunk --accept-license --answer-yes"
    #SPLUNK_CMD_ENV="export SPLUNK_HOME=$SPLUNK_HOME ; $SPLUNK_CMD"
    SPLUNK_CMD_ENV="$SPLUNK_CMD"
    #rename arguments
    UPDATE_HOST=$1
    #VERSION=$2
    #DISTRIBUTION=$3
    #PLATFORM=$4
    ##BITS=$5
    #SPLUNK_EXTENSION=$5
    LOGFILE="$LOGFILE_PREFIX-$UPDATE_HOST.log"

    #create stage dir
    STAGE_DIR="stage"
    run $LOGFILE $SSH $USER@$UPDATE_HOST mkdir $STAGE_DIR
    
    #if [ $PLATFORM = "Linux" ] ; then
    #    EXTENSION=.tgz
    #elif [ $PLATFROM = "solaris" ] ; then
    #    EXTENSION=.tar.Z
    #elif [ $PLATFROM = "aix" ] ; then
    #    EXTENSION=.tgz 
    
      
    #get install file
    if [ $UPLOAD_SPLUNK_FILE = false ] ; then
        SPLUNK_URL="$RELEASES_URL/released_builds/$VERSION/$DISTRIBUTION/$PLATFORM/"
        #SPLUNK_EXTENSION="$PLATFORM-$BITS-$EXTENSION"
    
        #declare pkg locations the cmds to retrieve them
        SPLUNK_FETCH_CMD="wget -nv --no-directories -r -l 1 --no-parent -A $SPLUNK_EXTENSION $SPLUNK_URL -P $STAGE_DIR"
        
        #fetch install from sc-build server
        run $LOGFILE $SSH $USER@$UPDATE_HOST $SPLUNK_FETCH_CMD
    else
        #upload install file
        run $LOGFILE $SCP $INSTALL_SPLUNK_FILE $USER@$UPDATE_HOST:$STAGE_DIR/
        #if [[ $INSTALL_SPLUNK_FILE == *.tgz ]] ; then
        #    EXTENSION=".tgz"
        #elif [[ $INSTALL_SPLUNK_FILE == *.tar.Z ]] ; then
        #    EXTENSION=".tar.Z"
        #else
        #    echo "Unknown install file type. Upload .tgz or .tar.Z file."
        #    exit 1
        #fi
    fi
    #install Splunk
    #run $LOGFILE $SSH $USER@$UPDATE_HOST $SPLUNK_CMD_ENV start
    #if [ "$APP_EXTENSION" = ".spl" ] ; then 
    #    run $LOGFILE $SSH $USER@$UPDATE_HOST $SPLUNK_CMD_ENV install app "$STAGE_DIR/*$APP_EXTENSION" -update true -auth admin:changeme
    #elif [ "$APP_EXTENSION" = ".zip" ] ; then
    #    #unzip should result in etc/ folder. You have to change it if the app is packaged otherwise 
    #    run $LOGFILE $SSH $USER@$UPDATE_HOST unzip $STAGE_DIR/*$APP_EXTENSION -d $SPLUNK_HOME/
    #elif [ "$APP_EXTENSION" = ".tar.gz" ] ; then
    #    #tar -xvf should result in etc/ folder. You have to change it if the app is packaged otherwise 
    #    run $LOGFILE $SSH $USER@$UPDATE_HOST gzip -d $STAGE_DIR/*tar.gz
    #    run $LOGFILE $SSH $USER@$UPDATE_HOST tar -xvf $STAGE_DIR/*.tar -C $SPLUNK_HOME/etc/apps/
    #fi
    run $LOGFILE $SSH $USER@$UPDATE_HOST gzip -d $STAGE_DIR/*
    run $LOGFILE $SSH $USER@$UPDATE_HOST tar -xvf $STAGE_DIR/* -C $STAGE_DIR
    
    run $LOGFILE $SSH $USER@$UPDATE_HOST $SPLUNK_CMD_ENV stop
    run $LOGFILE $SSH $USER@$UPDATE_HOST mkdir -p $SPLUNK_HOME
    run $LOGFILE $SSH $USER@$UPDATE_HOST cp -r $STAGE_DIR/$DISTRIBUTION/* $SPLUNK_HOME
    run $LOGFILE $SSH $USER@$UPDATE_HOST rm -r -f $STAGE_DIR
    
    #Setup
    
    # Set ports
    echo "Set http port = $WEB_PORT, management port = $SPLUNKD_PORT"
    echo -e "[settings]\nhttpport = $WEB_PORT\nmgmtHostPort = 127.0.0.1:$SPLUNKD_PORT" > web.temp
    run $LOGFILE $SCP web.temp $USER@$UPDATE_HOST:$SPLUNK_HOME/etc/system/local/web.conf
    rm -f web.temp
    
    if $UPDATE_LIC ; then
        #update licenses
        echo "Upload and add licensels
        $LIC_FILE_NAME"
        run $LOGFILE $SCP $LIC_FILE_NAME $USER@$UPDATE_HOST:$STAGE_DIR/
        run $LOGFILE $SSH $USER@$UPDATE_HOST $SPLUNK_CMD_ENV add licenses $STAGE_DIR/$LIC_FILE_NAME -auth admin:changeme
    fi
    
    #read-only IFS for restoring
    roIFS=$IFS
    
    if $ENABLE_LISTEN_PORT ; then
    #enable listen port
        echo "Eable listen port(s) $LISTEN_PORT_ENABLE"
        IFS=,
        ary=($LISTEN_PORT_ENABLE)
        IFS=$roIFS
        for key in "${!ary[@]}"; do
            run $LOGFILE $SSH $USER@$UPDATE_HOST $SPLUNK_CMD_ENV enable listen ${ary[$key]} -auth admin:changeme
        done
    fi
    
    if $DISABLE_LISTEN_PORT ; then
    #disable listen port
        echo "Disable listen port(s) $LISTEN_PORT_DISABLE"
        IFS=,
        ary=($LISTEN_PORT_DISABLE)
        IFS=$roIFS
        for key in "${!ary[@]}"; do
            run $LOGFILE $SSH $USER@$UPDATE_HOST $SPLUNK_CMD_ENV disable listen ${ary[$key]} -auth admin:changeme
        done
        IFS=
    fi
    
    if $ADD_FORWARD_SERVER ; then
    #add forward server
        echo "Add forward server(s) $FORWARD_SERVER_ADD"
        IFS=,
        ary=($FORWARD_SERVER_ADD)
        IFS=$roIFS
        for key in "${!ary[@]}"; do
            run $LOGFILE $SSH $USER@$UPDATE_HOST $SPLUNK_CMD_ENV add forward-server ${ary[$key]} -auth admin:changeme
        done
    fi
    
    if $REMOVE_FORWARD_SERVER ; then
    #remove forward server
        echo "Remove forward server(s) $FORWARD_SERVER_REMOVE"
        IFS=,
        ary=($FORWARD_SERVER_REMOVE)
        IFS=$roIFS
        for key in "${!ary[@]}"; do
            run $LOGFILE $SSH $USER@$UPDATE_HOST $SPLUNK_CMD_ENV remove forward-server ${ary[$key]} -auth admin:changeme
        done
    fi
    
    # Set inputs.conf
    echo "Set inputs.conf with compressed=$COMPRESSED_INPUT"
    run $LOGFILE $SCP $USER@$UPDATE_HOST:$SPLUNK_HOME/etc/apps/search/local/inputs.conf inputs.temp
    #sed '/[splunktcp]/a\compressed=$COMPRESSED_INPUT/' inputs.temp > inputs.temp
    echo "\n[splunktcp]\ncompressed=$COMPRESSED_INPUT\n" > inputs.temp
    run $LOGFILE $SCP inputs.temp $USER@$UPDATE_HOST:$SPLUNK_HOME/etc/apps/search/local/inputs.conf
    rm -f inputs.temp
    
    # Set outputs.conf
    echo "Set outputs.conf with compressed=$COMPRESSED_OUTPUT, useACK=$USE_ACK, autoLB=$AUTO_LB"
    run $LOGFILE $SCP $USER@$UPDATE_HOST:$SPLUNK_HOME/etc/system/local/outputs.conf outputs.temp
    echo "\n[tcpout]compressed=$COMPRESSED_OUTPUT\nuseACK=$USE_ACK\nautoLB=$AUTO_LB\n" > outputs.temp
    run $LOGFILE $SCP outputs.temp $USER@$UPDATE_HOST:$SPLUNK_HOME/etc/system/local/outputs.conf
    rm -f outputs.temp
    
    echo "Start Splunk..."
    run $LOGFILE $SSH $USER@$UPDATE_HOST $SPLUNK_CMD_ENV start
    
    
    
    echo "$UPDATE_HOST: Upgrading Complete. If there is no failure, Splunk is installed and running!"
}

main() {
    # update the idx hosts
    echo "Deploying on $HOSTS...( This takes a couple of minutes. )"
    for i in $HOSTS ; do
        mkdir -p $PWD/logs
        rm "$LOGFILE_PREFIX-$APP_NAME-on-$i.log" 2> /dev/null
        #run $i echo updating host: $i
        #update_idx_hosts $i $VERSION $DISTRIBUTION $PLATFORM $SPLUNK_EXTENSION &
        update_idx_hosts $i&
    done
    wait $!
    
    echo "Processes comeplete. Check logs for deployment results."
    exit 0
}

UPDATE_LIC=false
ENABLE_LISTEN_PORT=false
DISABLE_LISTEN_PORT=false
UPLOAD_SPLUNK_FILE=false
ADD_FORWARD_SERVER=false
REMOVE_FORWARD_SERVER=false
COMPRESSED_OUTPUT=false
COMPRESSED_INPUT=false
USE_ACK=false
AUTO_LB=true
while [[ $1 = -* || $1 = +* ]]; do
    PARAM=`echo $1 | awk -F= '{print $1}'`
    VALUE=`echo $1 | awk -F= '{print $2}'`
    case $PARAM in
        -l | --licence)
            UPDATE_LIC=true
            LIC_FILE_NAME=$2
            ;;
        +r | --enable-listen-port)
            ENABLE_LISTEN_PORT=true
            LISTEN_PORT_ENABLE=$2
            ;;
        -r | --disable-listen-port)
            DISABLE_LISTEN_PORT=true
            LISTEN_PORT_DISABLE=$2
            ;;
        -p | --web-port)
            WEB_PORT=$2
            ;;
        -d | --splunkd-port)
            SPLUNKD_PORT=$2
            ;;
        -f | --file)
            UPLOAD_SPLUNK_FILE=true
            INSTALL_SPLUNK_FILE=$2
            ;;
        -c | --fetch)
            VERSION=$2
            DISTRIBUTION=$3
            PLATFORM=$4
            SPLUNK_EXTENSION=$5
            shift 3 # shift 5 in total
            ;;
        +o | --forward-server)
            ADD_FORWARD_SERVER=true
            FORWARD_SERVER_ADD=$2
            ;;
        -o | --forward-server)
            REMOVE_FORWARD_SERVER=true
            FORWARD_SERVER_REMOVE=$2
            ;;
        -s | --path)
            SPLUNK_HOME=$2
            ;;
        --compressed-input)
            COMPRESSED_INPUT=$2
            ;;
        --compressed-output)
            COMPRESSED_INPUT=$2
            ;;
        --useack)
            USE_ACK=$2
            ;;
        --autolb)
            AUTO_LB=$2
            ;;
        -h | --help)
            echo 'This script deploys ES, PCI, Websphere, HadoopOps, Web Intelligence, and Unix.'
            echo 'To run the script, run'
            echo './deploy_es.sh <OPTIONS> (es|pci|websphere|hadoopops|webintelligence|unix) <HOST> '
            echo 'you can enter a list of hosts'
            echo 'Sample command: '
            echo '    ./deploy.sh -l 5TB.lic -p 8001 -f es.spl es qasus-centos-5 qasus-centos-6'
            echo ''
            echo 'Options:'
            echo '    -l | --licence <LICENSE FILE>    :  update license'
            echo '    -r | --listen-port <PORT>        :  add a listen/receiving port'
            echo '    -o | --forward-server <HOST:PORT>:  add a forward server'
            echo "    -p | --web-port <PORT>           :  set web port (default $WEB_PORT)"
            echo "    -d | --splunkd-port <PORT>       :  set management port (default $SPLUNKD_PORT)"
            echo '    -f | --file <INSTALL FILE>       :  upload splunk install file'
            echo '    -s | --path <PATH>               :  set splunk home path (default $SPLUNK_HOME)'
            echo '    -h | --help                      :  Help Menu'
            echo ''
            echo 'Note: ES, PCI and Websphere supports automatic installation.'
            echo 'HadoopOps, Web Intelligence, and Unix supports semi-automatic installation.'
            echo '(you need to click when you see hints.)'

            exit 1
            ;;
        *)
            echo "ERROR: unknown parameter \"$PARAM\""
            exit 1
            ;;
    esac
        shift 2
done

ARGV=($@)

#if <HOST> is defined, it gives a list of hosts
#if [ ${ARGV[1]} = non-cluster-indexers ] ; then
#    IDX_HOSTS=" \
#    qasus-centos-5 \
#    qasus-centos-6 \
#    qasus-centos-7 \
#    qasus-centos-8 \
#    qasus-centos-9 "
#else
#    
#    IDX_HOSTS=${ARGV[@]:4}
#fi
#VERSION=${ARGV[0]}
#DISTRIBUTION=${ARGV[1]}
#PLATFORM=${ARGV[2]}
#SPLUNK_EXTENSION=${ARGV[3]}
#HOSTS=${ARGV[@]:4}
HOSTS=${ARGV[@]:0}
# go...
main
