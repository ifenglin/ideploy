"""
Meta
====
    $Id: //splunk/solutions/nix/mainline/test/build/build_nix.py#12 $
    $DateTime: 2011/03/20 23:04:29 $
    $Author: dzakharov $
    $Change: 96913 $
"""

import logging
import os
from BuildUtil import BuildUtil


class TestBuildNix:

    # in this method we just build Nix
    def test_build(self):
        self.logger = logging.getLogger('BuildNix')
        codeline = os.environ["CODELINE"]
        buildutil = BuildUtil('unix', codeline, 'zip', self.logger)
        buildutil.build_solution()
