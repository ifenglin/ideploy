Splunk.Module.UnixAlertsFilter = $.klass(Splunk.Module, { 

    initialize: function ($super, container) {
        $super(container);

        this.namespace = this.getParam('namespace');
        this.token = this.getParam('token');

        this.filter = null;
        this.$filter = $('input#keyword_filter', this.container);

        this.initBindings();
    },

    getToken: function() {
        return this.token;
    },

    onContextChange: function() {
        var context = this.getContext(),
            value = context.get(this.namespace + '.' + this.token) || null;

        if (value !== null && value !== undefined) {
            this.updateFilter(value);
        }
    },

    getModifiedContext: function() {
        var context = this.getContext();

        context.set(this.namespace + '.' + this.token, this.getFilter());

        return context;
    },

    updateFilter: function(val) {
        this.filter = val;
        this.$filter.val(val);
    },

    initBindings: function($super) {
        var that = this;

        this.$filter.change(function(e) {
            var $elm = $(this),
                val = $elm.val();

            that.updateFilter(val);
        });
    },

    getFilter: function() {
        return this.filter;
    }

});

