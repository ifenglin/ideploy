from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from os import environ
import unittest, time, re

class InstallWi(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(180)
        self.base_url = "https://%s.sv.splunk.com:%s" % ( environ['DEPLOY_HOST'], environ['HTTP_PORT'] )
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_install_wi(self):
        driver = self.driver
        # ERROR: Caught exception [ERROR: Unsupported command [setTimeout | 120000 | ]]
        driver.get(self.base_url + "")
        driver.find_element_by_id("username").clear()
        driver.find_element_by_id("username").send_keys("admin")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("changeme")
        driver.find_element_by_css_selector("input.splButton-primary").click()
        driver.find_element_by_id("applicationsMenuActivator").click()
        driver.find_element_by_link_text("Splunk App for Web Intelligence").click()
        #driver.find_element_by_link_text("Configure").click()
        print "%s: Please Configure the App." % environ['DEPLOY_HOST']
        time.sleep(120)
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert.text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
