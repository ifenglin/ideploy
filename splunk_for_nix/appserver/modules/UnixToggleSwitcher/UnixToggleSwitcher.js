Splunk.Module.UnixToggleSwitcher = $.klass(Splunk.Module, { 

    initialize: function($super, container) {
        $super(container);
        this.index = 0;
                
        this.$buttons = $('a.chart', this.container);

        this.initBindings();
    },

    onLoadStatusChange: function($super, statusInt) {
        $super(statusInt);
        if (statusInt > Splunk.util.moduleLoadStates.WAITING_FOR_HIERARCHY) {
            this.hideDescendants(this.moduleId);
            this._children[this.index].show(this.moduleId);
            this._children[this.index].showDescendants(this.moduleId);
        }
    }, 
 
	initBindings: function($super) {
        var that = this;

        this.$buttons.click(function(e) {
            var $elm = $(e.target),
                id = $elm.attr('id');

            e.preventDefault();
             
            $elm.siblings().removeClass('selected');
            $elm.addClass('selected');
            
            //todo: make it generic by matching this._children
            if (id === 'cubism') {	
    			this.selectChart(0);    
            } else {
            	this.selectChart(1);
            }
          
        }.bind(this));
    },

	selectChart: function(selectedIndex) {
	
		if ( (this.index != selectedIndex) && (this.index >= 0) && (this.index < this._children.length) ) {
            this._children[this.index].hide(this.moduleId);
            this._children[this.index].hideDescendants(this.moduleId);
            this.index= selectedIndex;
            this._children[this.index].show(this.moduleId);
            this._children[this.index].showDescendants(this.moduleId);
        }
	}

});

