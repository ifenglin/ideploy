Splunk.Module.UnixAlertsViewer = $.klass(Splunk.Module, { 

    SEV_MAP: {'info': 1, 'low': 2, 'medium': 3, 'high': 4, 'critical': 5},

    initialize: function ($super, container) {
        $super(container);

        this.app_namespace = this.getParam('app_namespace'); 
        this.count = parseInt(this.getParam('count'));
        this.drilldown_view = this.getParam('drilldown_view');
        this.namespace = this.getParam('namespace');

        this.check_interval = parseInt(this.getParam('check_interval'))*1000;
        this.control = null;
        this.filter = null;
        this.interval = null;
        this.severity = null;

        this.$message = $('.message', this.container);
        this.$table = $('.splUnixAlertsTable', this.container);
        this.$tablebody = $('tbody', this.container);
    },

    onContextChange: function() {
        var context = this.getContext();
  
        this.control = context.get(this.namespace + '.alert_control') || 'play';
        this.filter = context.get(this.namespace + '.alert_filter') || null;
        this.severity = context.get(this.namespace + '.alert_severity') || null;

        if (this.control === 'play') {
            this.startInterval();
        } else {
            this.stopInterval();
        }
    },

    getResultParams: function($super) {
        var params = $super(),
            severities = [], i;

        params['count'] = this.count;
        params['app_namespace'] = this.app_namespace;

        if (this.filter !== null) {
            params['filter'] = this.filter;
        }

        if (this.severity !== null) {
            for (i=0; i < this.severity.length; i++) {
                severities.push(this.SEV_MAP[this.severity[i]]);
            }
            params['severity'] = severities.join(',');
        }

        return params;
    },

    startInterval: function() {
        if (this.interval !== null) {
            clearInterval(this.interval);
            this.interval = null;
        }
        this.interval = setInterval(this.getResults.bind(this), this.check_interval);
        this.getResults();
    },

    stopInterval: function() {
        if (this.interval !== null) {
            clearInterval(this.interval);
            this.interval = null;
        }
    },

    renderResults: function (data) {
        if (data === null || data === undefined) {
            this.$table.hide();
            this.$message.text('Waiting for headlines...'); 
            this.$message.show();
        } else if (data.alerts !== null && data.alerts !== undefined) {
            if (data.alerts.length === 0) {
                this.$table.hide();
                this.$message.text('No alerts found');
                this.$message.show();
            } else {
                this.$message.hide();
                this.$tablebody.children().remove();
                this.$table.show();
                this.addTableRows(data.alerts);
            }
        } else if (data.error !== null || data.error !== undefined) {
            this.$message.text(data.error);
            this.$table.hide();
            this.$message.show();
        } 
    },

    addTableRows: function (results) {
        var app = Splunk.util.getCurrentApp(),
            uri = Splunk.util.make_url('app', app, this.drilldown_view),
            $new_row, param, result;

        for (var i = 0; i < Math.min(results.length, this.count); i++) {
            $new_row = $('<tr>');
            result = results[i];
            if (result['name'] !== null && result['name'] !== undefined) {
                $('<td>').text(result['name'])
                    .addClass('alert')
                    .addClass('severity' + result['severity'].toString())
                    .appendTo($new_row);
            }
            if (result['timesince'] !== null && result['timesince'] !== undefined) {
                $('<td>').text(result['timesince'] + ' ago')
                    .addClass('date')
                    .appendTo($new_row);
            }
            $new_row.children().bind("click", function() { 
                param = {'s': result['job_id']};
                uri += '?' + Splunk.util.propToQueryString({'sid': result['job_id']}); 
                window.location = uri;
            }).css('cursor','pointer');
            $new_row.appendTo(this.$tablebody);
        }
    }

});

