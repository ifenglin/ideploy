unixcf = {};

unixcf.time_of_day = {
    dimension: function(d) { return d.date.getHours() + d.date.getMinutes() / 60; },
    group: Math.floor,
    x: function (dim, grp, params) { return d3.scale.linear().domain([0, 24]).rangeRound([0, params.barWidth * 24]); }
};

unixcf.by_host = {
    dimension: function(d) { return d.host; },
    x: function(dim, grp, params) {
        var groups = grp.all(),
            hosts = [];

        for (var i = 0; i < groups.length; i++) {
            hosts.push(groups[i].key);
        }

        return d3.scale.ordinal().domain(hosts).range([0, params.barWidth * hosts.length]);
    }
};

unixcf.date = {
    dimension: function(d) { return d3.time.day(d.date); },
    x: function(dim, grp, params) {
        var span = 60,
            now = d3.time.day(new Date()),
            end = d3.time.day.offset(now, 1),
            start = d3.time.day.offset(now, 1 - span),
            bw = (params && params.barwidth) || 10;

        return d3.time.scale()
            .domain([start, end])
            .rangeRound([0, bw * span]);
    }
    //filter: [new Date(year, month, day - 7), new Date(year, month, day + 1)]
};

unixcf.by_name = {
    dimension: function(d) { return d.ss_name; }
};

unixcf.by_severity = {
    dimension: function(d) { return d.severity; }
};

unixcf.scroller = {
    dimension: function(d) { return d.ss_name; }
};
