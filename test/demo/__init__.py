'''
Copyright (C) 2005-2012 Splunk Inc. All Rights Reserved.
'''
import os, sys
import logging

#Setup logging for this test run
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(levelname)-5s  %(message)s',
                    filename='nix-test.log',
                    filemode='w')

logger = logging.getLogger('nix-python-fw')
logger.info('Logging setup completed.')
