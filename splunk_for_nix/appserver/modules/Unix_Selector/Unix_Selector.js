
Splunk.Module.Unix_Selector = $.klass(Splunk.Module.DispatchingModule, {

    initialize: function($super, container) {
    
    
    	
    
        $super(container);
        this.data = null;
        this.$list = $('ol#selector', this.container);
        this.namespace = this.getParam('namespace');
        this.selector = null;
        this.selected = null;
        this.spl = [];
        this.token = this.getParam('token');
    },

    /*
     * build list from controller data
     */
    buildList: function() {
        this.$list.children().remove();
        for (var i=0; i < this.data.length; i++) {
            this.buildListItem(this.data[i]);
        }
    },

    /*
     * build individual list items
     */
    buildListItem: function(item) {
    
    
        if(item.indexOf("&") > 0) {
        
            this.spl.push(item);
        
            var temp =item.split("&");
        	item = temp[0];
        	
        } 
        
        
        if (item.charAt(0) === '#' ) {
        
        	 var tag = item.split("#");
        	 
        	 var folder =tag[1]; 
        	 
        	 item = tag[2];
        
        	 var $elm = $('<li>').addClass('ui-widget-content foldee ' + folder)
            .attr('id', item)
            .attr('title', item);
                 	
        }
        else {    
            var $elm = $('<li>').addClass('ui-widget-content')
            .attr('id', item)
            .attr('title', item);
        }
        	
        $('<span>').text(item).appendTo($elm);
        
        $elm.appendTo(this.$list);
    },



    /*
     * deselect an item by removing it from selected 
     */
    deselectItem: function(elm) {
        var $elm = $(elm),
            item = $elm.attr('id'),
            selected = this.selected,
            idx;
        if ($elm.is('li') === false) {
            return;
        }
        if (selected !== null) {
            idx = $.inArray(item, selected);
            if (idx > -1) {
                selected.splice(idx, 1);
            }
            
            //remove from search command
            /**
            for(var i=0; i < this.spl.length; i ++) {
            	
				if (this.spl[i].indexOf(item) > 0) {
					
					this.spl.splice(i, 1);
				}            
            } **/

        }
        this.selected = selected;
    },

    /*
     * destroy the selectable() 
     */
    destroySelectable: function() {
        if (this.selector !== null) {
            this.selector.destroy();
            this.selector = null;
        }
    },

    /*
     * override
     * insert selected values into our given token and namespace
     */
    getModifiedContext: function() {
    
    
    
        var context = this.getContext(),
            namespace = context.get(this.namespace) || {},
            search = context.get('search');

		
		
        search.abandonJob();
        
        //alert("after abandon");
        
        if (this.selected !== null && this.selected.length > 0) {
            namespace[this.token] = this.selected;
            namespace['cmd'] = this.spl;
            
        } else {
            namespace[this.token] = null;
        }
        
        //alert("after null");
        
        context.set(this.namespace, namespace);
        context.set('search', search);
        
        //alert("after search");
        return context;
    },

    /*
     * override
     * provide the sid and token to the controller
     */
    getResultParams: function($super) {
        var params = $super();
        params['sid'] = this.getContext().get('search').job.getSID();
        params['token'] = this.getToken();
        return params;
    },

    /*
     * override
     * we have no intentions, just a token
     */
    getToken: function() {
        return this.token;
    },

    /*
     * initialize the list as a jquery ui selectable 
     */
    initSelectable: function() {
        // if exist, destroy , todo: need to find when need to destroy preexisting selection 
        //this.destroySelectable();

        // build list from controller data

        this.buildList(); 

        // make list selectable
        this.selector = this.$list.selectable();

        // bind to select/deselect events
        this.selector.bind(
            'selectableselected', 
            this.onSelect.bind(this)
        ).bind(
            'selectableunselected',
            this.onDeselect.bind(this)
        );
    },

    /*
     * override
     * grab any selected values from the context 
     */
    onContextChange: function($super) {
        var context = this.getContext(),
            namespace = context.get(this.namespace) || {},
            token = namespace[this.token] || null;

        if (token !== null) {
            this.selected = token;
            this.selectSelected();
        } 
	
		//alert("super");
       // $super();
        //alert("after super");
    },

    /*
     * callback for selectable item deselection 
     */
    onDeselect: function(e, ui) {
        $(ui.unselected).addClass('ui-selected');
        console.log('deselect', ui.unselected);
        e.preventDefault();
    },

    /*
     * get results
     */
    onJobDone: function() {
        this.getResults();
    },

    /*
     * callback for selectable item selection 
     */
    onSelect: function(e, ui) {
        
        this.selectItem(ui.selected); 
        
        console.log('push this.selected: ', this.selected);
        console.log('push this.spl', this.spl);
        console.log('push this.context', this.getContext());
        
        this.pushContextToChildren(); 
        
    },
    

    /*
     * override
     * send valid data to selectable 
     */
    renderResults: function(data) {
        if (data !== null && data !== undefined
          && data.results !== null && data.results !== undefined) {
            this.data = data.results;
            if (this.selector === null) {
                this.initSelectable();
            } else {
                //this.updateSelectable();
            }
        }
    },


    /*
     * select an item by adding it to selected
     * if it is already in selected, it needs to be deselected
     */
    selectItem: function(elm) {
        var $elm = $(elm),
            item = $elm.attr('id'),
            selected = this.selected,
            idx;
        
        
         var child = this.$list.find('.' + item);
         
         
         for (var i =0; i < child.length; i++) {
         
         	if ($(child[i]).hasClass('show')) {
         	 	$(child[i]).removeClass('show');
         	} else {
         		$(child[i]).addClass('show');
         	}
         	
         }
         
         
        if ($elm.is('li') === false) {
            return;
        }
        
        //console.log('selected', item, $elm);
        if (selected === null) {
            selected = [item];
        } else {
            idx = $.inArray(item, selected);
            if (idx === -1) {
                selected.push(item);
            } else {
            
            	/**
                for (var i =0; i < child.length; i++) {
         
         		    $(child[i]).removeClass('show');
         		} **/
         		
         	    var tmp = this.$list.find('.' + item+'.ui-selected' );
         		
         		if (tmp.length>0  ) {
         		   $elm.addClass('contain');
         		} else {
         		   $elm.removeClass('contain');
         		   $elm.removeClass('ui-selected');
         		   this.deselectItem(elm); 
         		}
                        
                return;
                
            }
        }
        this.selected = selected;
    },
    

    /*
     * select any items that were given to us in the context
     */
    selectSelected: function() {
        var selected = this.selected,
            child, i, item;

        // reset selected and remove selected class
        this.selected = null;
        this.$list.children().removeClass('ui-selected');

        for (i=0; i < selected.length; i++) {
            item = selected[i];
            child = this.$list.find('#' + item);
            
            if (child.length > 0) {
                $(child[0]).addClass('ui-selected');
                this.selectItem(child[0]);
            }
        } 
    },

    /*
     * update selectable
     */
    updateSelectable: function() {
    
    	//alert("update");
        this.buildList();
        this.selector.refresh();
    }

});
