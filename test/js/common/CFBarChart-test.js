require("./env");

var vows = require("vows"),
    assert = require("assert");

var suite = vows.describe("splunk_for_nix.CFBarChart");
//$(document).triggerHandler('ready');

suite.addBatch({
    "CFBarChart": {
        topic: function() {
            d3.select('body').append('div');
            return new Splunk.Module.CFBarChart($('div'));
        },
        "initialize": function() {
            assert.equal(d3.select('div').empty(), false);
        }
    },

    "barChart" : {
        topic: function() {
            return barChart('barchart1');
        },
        "barchart create": function(chart) {
            assert.equal(typeof chart.dimension, 'function');
            assert.equal(typeof chart.group, 'function');
        }
    }
});

suite.export(module);
