//put Module in the namespace if it isnt already there.
Splunk.namespace("Module");

Splunk.Module.DebugNullModule = $.klass(Splunk.Module, {
    initialize: function($super, container) {
        $super(container);
        this.hide(this.HIDDEN_MODULE_KEY);
        this.name = this.getParam('name');
        if (this.name)
            this.logger = Splunk.Logger.getLogger('debug_null_' + this.name, Splunk.Logger.mode.console);
        else
            this.logger = console;
    },

    onContextChange: function() {
        this.logger.debug('onContextChange');
        this.logger.debug("context", this.getContext());
    }
});
