#!/bin/bash

DIR=$SPLUNK_HOME/etc/apps/splunk_for_nix/bin

$DIR/cpu.sh
$DIR/df.sh
$DIR/hardware.sh
$DIR/interfaces.sh
$DIR/iostat.sh
$DIR/lastlog.sh
$DIR/lsof.sh
$DIR/netstat.sh
$DIR/openPorts.sh
$DIR/package.sh
$DIR/protocol.sh
$DIR/ps.sh
$DIR/rlog.sh
$DIR/time.sh
$DIR/top.sh
$DIR/usersWithLoginPrivs.sh
$DIR/vmstat.sh
$DIR/who.sh
