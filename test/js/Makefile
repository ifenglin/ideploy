NODE_PATH ?= ./node_modules
JS_COMPILER = $(NODE_PATH)/uglify-js/bin/uglifyjs
JS_BEAUTIFIER = $(NODE_PATH)/uglify-js/bin/uglifyjs -b -i 2 -nm -ns
JS_TESTER = $(NODE_PATH)/vows/bin/vows
LOCALE ?= en_US

# find source path, a bit overkill
where-am-i = $(CURDIR)/$(word $(words $(MAKEFILE_LIST)),$(MAKEFILE_LIST))
THIS_MAKEFILE := $(call where-am-i)
APPDIR ?= $(dir $(THIS_MAKEFILE))../..
MODULE_PATH = $(abspath $(APPDIR)/apps/splunk_for_nix/appserver/modules)

ifndef MRSPARKLE
$(error MRSPARKLE undefined.)
endif

MRSPARKLE_MODULES = $(MRSPARKLE)/modules
MRSPARKLE_EXPOSED = $(MRSPARKLE)/exposed/js

# copied from windows-view.html
coreJsFiles = $(subst /static/js, $(MRSPARKLE_EXPOSED), \
/static/js/contrib/lowpro_for_jquery.js \
/static/js/contrib/json2.js \
/static/js/contrib/swfobject.js \
/static/js/contrib/firebug.js \
/static/js/contrib/jquery-ui-1.8.16.custom.min.js \
/static/js/contrib/jquery.ui.tablesorter.js \
/static/js/contrib/jquery.bgiframe.min.js \
/static/js/contrib/jquery.cookie.js \
/static/js/contrib/jquery.form.js \
/static/js/contrib/script.js \
/static/js/contrib/strftime.js \
/static/js/i18n.js \
/static/js/splunk.js \
/static/js/util.js \
/static/js/logger.js \
/static/js/error.js \
/static/js/session.js \
/static/js/job.js \
/static/js/messenger.js \
/static/js/jobber.js \
/static/js/menu_builder.js \
/static/js/admin.js \
/static/js/time_range.js \
/static/js/module_loader.js \
/static/js/ja_bridge.js \
/static/js/legend.js \
/static/js/jquery.sparkline.js \
/static/js/popup.js \
/static/js/layout_engine.js \
/static/js/print.js \
/static/js/page_status.js \
/static/js/dev.js \
/static/js/window.js \
/static/js/field_summary.js \
/static/js/splunk.jquery.csrf_protection.js \
/static/js/splunk.jquery.check_messages.js \
/static/js/init.js)

all: \
	component.json \
	package.json \
	splunk.core.js \
	splunk.core.test_utils.js \
	splunk_for_nix.v1.js \
	splunk_for_nix.v1.min.js

# Modify this rule to build your own custom release.

.INTERMEDIATE splunk_for_nix.v1.js: \
	build/splunk_start.js \
	build/start.js \
	splunk_for_nix.CFBarChart.js \
	build/end.js

splunk.core.test_utils.js: \
	$(MRSPARKLE_EXPOSED)/test_utils.js

splunk.core.js: \
	$(coreJsFiles) \
	$(MRSPARKLE_MODULES)/AbstractModule.js

splunk_for_nix.Unix_Crossfilter.js: \
	$(MODULE_PATH)/Unix_Crossfilter/Unix_Crossfilter.js

splunk_for_nix.CFBarChart.js: \
	$(MODULE_PATH)/CFBarChart/barchart.js \
	$(MODULE_PATH)/CFBarChart/CFBarChart.js

test: all
	@$(JS_TESTER)

%.min.js: %.js Makefile
	@rm -f $@
	$(JS_COMPILER) < $< > $@

splunk.core.%.js: Makefile
	@rm -f $@
	for js in $(filter %.js,$^); do \
		cat $$js | $(JS_BEAUTIFIER) >> $@ ;\
	done
	@chmod a-w $@

splunk.core.js: Makefile
	@rm -f $@
	for js in $(filter %.js,$^); do \
		cat $$js | $(JS_BEAUTIFIER) >> $@ ;\
	done
	echo 'module.exports = Splunk;' >> $@
	#@chmod a-w $@

splunk_for_nix%.js: Makefile
	@rm -f $@
	@echo $@ $^
	cat $(filter %.js,$^) | $(JS_BEAUTIFIER) > $@
	@chmod a-w $@

component.json: build/component.js
	@rm -f $@
	node build/component.js > $@
	@chmod a-w $@

package.json: build/package.js
	@rm -f $@
	node build/package.js > $@
	@chmod a-w $@
	@cd $(dir NODE_PATH) && npm install

clean:
	rm -f splunk*.js package.json component.json
