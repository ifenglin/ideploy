function cubismize(div, ctx) {
    var events = ctx.values,
        evtlen = ctx.size,
        step = ctx.step,
        serverDelay = ctx.serverDelay;

    if (events === undefined || evtlen === undefined || 
        step === undefined || serverDelay === undefined) {
        console.error("incomplete cubism stats data");
        return;
    }

    //console.log("events", events, context);

    if (!events)
        return;

    var metrics = [];

    var cubtext = cubism.context()
        .serverDelay(serverDelay - Date.now())
        .step(step)
        .size(evtlen)
        .stop();
    
    Object.keys(events).forEach(function(k) {
        var values = events[k];
        //console.log(k, d3.extent(values));
        metrics.push([cubtext.metric(function(start, stop, step, callback) {
            //console.log(values, k);
            callback(null, values.slice(-cubtext.size()));
        }, k), k, d3.extent(values)]);
    });

    var stats = div.append('div');

    stats.append("div")
        .attr("class", "axis")
        .call(cubtext.axis().orient("top"));

    stats.selectAll('div.horizon')
        .data(metrics)
      .enter().append('div')
        .attr("class", "horizon")
      .each(function(d, i) {
        d3.select(this).datum(d[0])
            .call(cubtext.horizon()
            .height(40)
            .colors(["#3182bd","#6baed6","#bdd7e7","#bae4b3","#74c476","#31a354"])
            .title(d[1])
            .extent(d[2]));
      });
}
