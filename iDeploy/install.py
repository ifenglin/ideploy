# I-Feng Lin, May 27th 2013
from splunkweb import SplunkWeb
from time import sleep
from os import system
from socket import error
from httplib2 import SSLHandshakeError
import sys

MAX_TRIAL = 7

def connect(host, port, protocol='http'):
    trial = 0
    while True:
        try:
            connection = SplunkWeb(hostpath = protocol+'://' + host + ':' + port)
            trial = 0
            return connection
            #response, content = splunkwebSSL.request('/en-US/app/SplunkEnterpriseSecurityInstaller/es_app_installer', 'GET',  {'Content-Type': 'charset=UTF-8'})
        except (error, SSLHandshakeError):
            # if http fails, try https instead, because http failure is not expected.
            if protocol=='http':
                protocol = 'https'
                continue
            trial += 1
            if trial > MAX_TRIAL:
                print "Too many failures. Good bye!"
                sys.exit(1)
            print "%s: Splunk is not ready yet. Sleep 15 more seconds... (%d/%d)" % (host, trial, MAX_TRIAL)
            sleep(15)
            
            
def install(app, port, hosts):
    if app == 'es':
        enable_all_correlation_searches = True
        need_configure = True
        request_install = '/en-US/custom/SplunkEnterpriseSecurityInstaller/es_installer_controller/show'
        request_type='POST'
        request_enable_correlation_searches = '/en-US/app/SplunkEnterpriseSecuritySuite/ess_correlation_search_list?'
        correlation_searches = [
            'correlation_search=Access+-+Brute+Force+Access+Behavior+Detected+-+Rule&correlation_search=Access+-+Cleartext+Password+At+Rest+-+Rule&correlation_search=Access+-+Completely+Inactive+Account+-+Rule&correlation_search=Access+-+Default+Account+Usage+-+Rule&correlation_search=Access+-+Default+Accounts+At+Rest+-+Rule&correlation_search=Access+-+Excessive+Failed+Logins+-+Rule&correlation_search=Access+-+High+or+Critical+Priority+Individual+Logging+into+Infected+Machine+-+Rule&correlation_search=Access+-+Inactive+Account+Usage+-+Rule&correlation_search=Access+-+Insecure+Or+Cleartext+Authentication+-+Rule&correlation_search=Audit+-+Anomalous+Audit+Trail+Activity+Detected+-+Rule&correlation_search=Audit+-+Expected+Host+Not+Reporting+-+Rule&correlation_search=Audit+-+Personally+Identifiable+Information+Detection+-+Rule&correlation_search=Audit+-+Potential+Gap+in+Data+-+Rule&correlation_search=Endpoint+-+Anomalous+New+Listening+Port+-+Rule&correlation_search=Endpoint+-+Anomalous+New+Processes+-+Rule&correlation_search=Endpoint+-+Anomalous+New+Services+-+Rule&correlation_search=Endpoint+-+High+Number+Of+Infected+Hosts+-+Rule&correlation_search=Endpoint+-+High+Number+of+Hosts+Not+Updating+Malware+Signatures+-+Rule&correlation_search=Endpoint+-+High+Or+Critical+Priority+Host+With+Malware+-+Rule&correlation_search=Endpoint+-+Host+With+Excessive+Number+Of+Processes+-+Rule&correlation_search=Endpoint+-+Host+Sending+Excessive+Email+-+Rule&correlation_search=Endpoint+-+Recurring+Malware+Infection+-+Rule&correlation_search=Endpoint+-+Host+With+Excessive+Number+Of+Listening+Ports+-+Rule&correlation_search=Endpoint+-+Host+With+Excessive+Number+Of+Services+-+Rule&correlation_search=Endpoint+-+Host+With+Multiple+Infections+-+Rule&action=enable'
            ]
    elif app == 'pci':
        enable_all_correlation_searches = True
        need_configure = True
        request_install = '/en-US/custom/SplunkPCIComplianceSuiteInstaller/pci_installer_controller/show'
        request_type='GET'
        request_enable_correlation_searches = '/en-US/app/SplunkPCIComplianceSuite/pci_correlation_search_list?enable='
        correlation_searches = [
            'PCI%20-%208.5%20-%20Account%20Deleted%20-%20Rule',
            'Access%20-%20Brute%20Force%20Access%20Behavior%20Detected%20-%20Rule',
            'Access%20-%20Cleartext%20Password%20At%20Rest%20-%20Rule',
            'Access%20-%20Completely%20Inactive%20Account%20-%20Rule',
            'Access%20-%20Default%20Account%20Usage%20-%20Rule',
            'Access%20-%20Default%20Accounts%20At%20Rest%20-%20Rule',
            'Access%20-%20Excessive%20Failed%20Logins%20-%20Rule',
            'Access%20-%20Inactive%20Account%20Usage%20-%20Rule',
            'Access%20-%20Insecure%20Or%20Cleartext%20Authentication%20-%20Rule',
            'Audit%20-%20Anomalous%20Audit%20Trail%20Activity%20Detected%20-%20Rule',
            'Audit%20-%20Expected%20Host%20Not%20Reporting%20-%20Rule',
            'Audit%20-%20Personally%20Identifiable%20Information%20Detection%20-%20Rule',
            'PCI%20-%206.1%20-%20Anomalous%20Update%20Service%20Detected%20-%20Rule',
            'PCI%20-%206.1%20-%20High/Critical%20Update%20Missing%20-%20Rule',
            'Endpoint%20-%20Recurring%20Malware%20Infection%20-%20Rule',
            'PCI%20-%205.2%20-%20Inactive%20Antivirus%20Client%20Detected%20-%20Rule',
            'PCI%20-%202.2.1%20-%20Multiple%20Primary%20Functions%20-%20Rule',
            'PCI%20-%205.1.1%20-%20Possible%20Outbreak%20Observed%20-%20Rule',
            'PCI%20-%202.2.4%20-%20Prohibited%20or%20Insecure%20Port%20Detected%20-%20Rule',
            'PCI%20-%202.2.4%20-%20Prohibited%20or%20Insecure%20Process%20Detected%20-%20Rule',
            'PCI%20-%202.2.4%20-%20Prohibited%20or%20Insecure%20Service%20Detected%20-%20Rule',
            'Endpoint%20-%20Should%20Timesync%20Host%20Not%20Syncing%20-%20Rule',
            'PCI%20-%201.1.4%20-%20Asset%20Ownership%20Unspecified%20-%20Rule',
            'PCI%20-%204.1%20-%20Credit%20Card%20Data%20Transmitted%20In%20Clear%20-%20Rule',
            'Network%20-%20Policy%20Or%20Configuration%20Change%20-%20Rule',
            'PCI%20-%201.2.2%20-%20Secure%20and%20synchronize%20router%20configuration%20files%20-%20Rule',
            'PCI%20-%2011.1%20-%20Rogue%20Wireless%20Device%20-%20Rule',
            'PCI%20-%202.2.2%20-%20System%20Misconfigured%20-%20Rule',
            'PCI%20-%201.2.3%20-%20Unauthorized%20Wireless%20Device%20Detected%20-%20Rule',
            'PCI%20-%201.3.3%20-%20Unauthorized%20or%20Insecure%20Communication%20Permitted%20-%20Rule',
            'PCI%20-%202.1.1%20-%20Unencrypted%20Traffic%20on%20Wireless%20Network%20-%20Rule',
            'Network%20-%20Vulnerability%20Scanner%20Detection%20(by%20event)%20-%20Rule',
            'Network%20-%20Vulnerability%20Scanner%20Detection%20(by%20targets)%20-%20Rule',
            'Threat%20-%20Watchlisted%20Events%20-%20Rule'
        ]
    else:
        print "Wrong argument. Please use \"es\" or \"pci\"."
        return
    for host in hosts:
        splunkweb = connect(host, port, 'http')
        splunkweb.request(request_install, 'POST', {'step': 'install'},  {'Content-Type': 'charset=UTF-8'})
        print "%s: We are about to install..." % host
        sleep(5)
        splunkweb.request('/en-US/api/manager/control', 'POST', {'operation': 'restart_server'},  {'Content-Type': 'charset=UTF-8'})
        print "%s: Restart Splunk. Sleep 60 seconds..." % host
        sleep(60)
        splunkwebSSL = connect(host, port, 'https')
        print "%s: Splunk is up." % host
        if need_configure:
            print "%s: Starting Selenium to confiugre..." % host
            system("export DEPLOY_HOST=%s; export HTTP_PORT=%s; python webdriver/configure_%s.py" % ( host, port, app.lower() ) )
            #response, content = splunkwebSSL.request('/en-US/manager/SplunkEnterpriseSecuritySuite/apps/local/SplunkEnterpriseSecuritySuite/SplunkEnterpriseSecuritySuite', 'POST',  {'Content-Type': 'charset=UTF-8'})
            print "%s: %s is configured." % (host, app)
        #enable correlation searches
        if enable_all_correlation_searches:
            print "%s: enabling all correlation searches...( This may take a couple of minutes. )" % host
            for cs in correlation_searches:
                splunkwebSSL.request(request_enable_correlation_searches + cs, request_type,  {'Content-Type': 'charset=UTF-8'})
            print "%s: All correlation searches are enabled." % host
        #splunkwebSSL.request('/en-US/api/manager/control', 'POST', {'operation': 'restart_server'},  {'Content-Type': 'charset=UTF-8'})
        #print "%s: Restart Splunk. Sleep 60 seconds..." % host
        #sleep(60)
        #connect(host, 'https')
        #print "%s: Splunk is up.  %s is installed successfully." % (host, app)
        #print "%s: Starting Selenium to test..." % host
        #system("export DEPLOY_HOST=%s; python test_%s.py" % ( host, app.lower() ) )
        #print "%s: If there is no failure, you are good to go!" % host
        
def main():
    argv = sys.argv
    install(argv[1], argv[2], argv[3:])
    return 0
    
if __name__ == '__main__':
    sys.exit(main())
    
    