Splunk.Module.BubbleSortChart = $.klass(Splunk.Module, {
    initialize: function ($super, container) {
        
        $super(container);

        this.div_id = '#' + this.moduleId + '_bubbleSortChart';
        this.svg_id = this.moduleId + '_svg';
    },
    
    displayChart: function(data){
        
        var div_id = this.div_id;
        
        var diameter = 960,
            format = d3.format(",d"),
            color = d3.scale.category10();
        
        var bubble = d3.layout.pack()
            .sort(null)
            .size([diameter, diameter]);
        //    .padding(1.5);
        
        var svg = d3.select(div_id).html('').append("svg")
            .attr("width", diameter)
            .attr("height", diameter)
            .attr("class", "bubble");
        
        var node = svg.selectAll(".node")
            .data(bubble.nodes(classes(data))
            .filter(function(d) { return !d.children; }))
            .enter().append("g")
            .attr("class", "node")
            .attr("transform", function(d) { return "translate("+ d.x +", " + d.y + ")"; });
      
        node.append("title")
            .text(function(d) { return d.className + ": " + format(d.value); });
      
        node.append("circle")
            .attr("r", function(d) { return d.r; })
            .style("fill", function(d) { return color(d.packageName); });
      
        node.append("text")
            .attr("dy", ".3em")
            .style("text-anchor", "middle")
            .text(function(d) { return d.className.substring(0, d.r / 3); });
      
        // Returns a flattened hierarchy containing all leaf nodes under the root.
        function classes(root) {
          var classes = [];
        
          function recurse(name, node) {
            if (node.children) node.children.forEach(function(child) { recurse(node.name, child); });
            else classes.push({packageName: name, className: node.name, value: node.size, x:node.positionX });
          }
        
          recurse(null, root);
          return {children: classes};
        }
        
        d3.select(self.frameElement).style("height", diameter + "px");
        
     },
    
    getDataByProcess: function(data, indexOnClick){
        var new_data = new Object();
        var items = {};
        var timeOnClick, value, colon_split, at_split, type, ps, host, max_cpu = 0.0, max_mem = 0.0;
        data = d3.csv.parse(data);
        
        if (data[indexOnClick] != undefined){
            timeOnClick = data[indexOnClick]['_time'];
        }
        
        data.forEach(function(obj) {
            for (var p in obj) {
                if (obj.hasOwnProperty(p)) {
                    if(p=='_time' && obj[p] == timeOnClick){ //store the data at time point
                        for (var q in obj) {
                            if (obj.hasOwnProperty(q) && q.charAt(0)=='#') {
                                value = ( obj[q] == undefined || obj[q] == "" || obj[q] == null) ? 0.0: parseFloat(obj[q]);
                                colon_split = q.split(': ');
                                at_split = colon_split[1].split('@');
                                type = colon_split[0].slice(1);
                                ps = at_split[0];
                                host = at_split[1];
                                if( !items.hasOwnProperty(host) ) {
                                    items[host] = {};
                                }
                                if (!items[host].hasOwnProperty(ps)){
                                    items[host][ps] = {};
                                }
                                items[host][ps][type] = value;
                            }
                        }
                    }
                }
            }
        });
        
        for(var host in items){
            for (var ps in items[host]){
                if (items[host][ps].pctCPU > max_cpu)
                    max_cpu = items[host][ps].pctCPU;
                if (items[host][ps].pctMEM > max_mem)
                    max_mem = items[host][ps].pctMEM;
            }
        }
        
        var new_data = {
            name: "All",
            children: []
        };
        
        for (var host in items){
            new_items = [];
            for (var ps in items[host]){
                val_cpu = items[host][ps].pctCPU == undefined? 0 : items[host][ps].pctCPU;
                val_mem = items[host][ps].pctMEM == undefined? 0 : items[host][ps].pctMEM;
                if ( val_cpu == 0 && val_mem == 0)
                    continue;
                new_items.push( {
                    name: ps,
                    size: val_mem*20/max_mem
                });
            }
            new_data.children.push({
                name: host,
                children: new_items
                });
        }
        
        return new_data;
    },

  
    getModifiedContext: function() {
    
        var that = this,
            context = this.getContext(),
            dataset = context.get('dataset');
    },

    onContextChange: function () { 
        
         var that = this,
            context = this.getContext(),
            dataset = context.get('dataset');

        var indexOnClick = context.get('indexOnClick');
        if (dataset != undefined) {
            this.displayChart(this.getDataByProcess(dataset.results,indexOnClick));
        }
    }
    

});