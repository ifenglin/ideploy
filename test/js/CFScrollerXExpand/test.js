curl(['../common/envs', 'domReady!'], function(envs) {
    var config = envs.config,
        core = envs.core,
        module_name = 'CFScrollerX',
        deps = [
            "js!appserver/static/d3.v2.min.js",
            "js!appserver/static/crossfilter.union.min.js",
            "js!appserver/static/custom.js",
            "js!modules/CFScrollerXExpand/CFScrollerXExpand.js",
            "js!modules/CFScrollerXExpand/expand.js",
            "js!my/data.js",
            "css!appserver/modules/CFScrollerXExpand/CFScrollerXExpand.css"
        ];

    var context;

    config.paths.my = 'CFScrollerXExpand';
    config.paths.appserver = config.paths.splunk + '/etc/apps/splunk_for_nix/appserver/';
    config.paths.modules = config.paths.appserver + '/modules/';

    curl(config, core)
        .next(deps)
        .then(
            function() {
                setup();
                process_cf_data();
            },
            function(ex) {
                console.log(ex);
            }
        );

    function setup() {
        var $script = function() {};
        
        context = new Splunk.Context();
        
        Splunk.Module.CFScrollerXExpand = $.klass(Splunk.Module.CFScrollerXExpand, {
          getContext: function() {
            return context;
          }
        });
        
        var target = $('#top');
        target.attach(Splunk.Module.CFScrollerXExpand);
        
        var div = d3.select('#top').data([mydata2]);
        context.set('cfscroller_data', {selection: d3.select('#top'), level: 2});
        
        target.triggerHandler('ContextChange');
    }
    
    function process_cf_data() {
        var div = d3.select('#top'),
            d = mydata,
            margin = {
                top: 5,
                right: 0,
                bottom: 30,
                left: 30
            },
            width = +div.style('width').slice(0, -2) - margin.left - margin.right,
            height = 80 - margin.top - margin.bottom;
    
        var base = {
            ps: {
                basesearch: 'index=os sourcetype=ps | head 1 | multikv | eval RSZ=RSZ_KB * 1024 | eval VSZ=VSZ_KB * 1024 | convert mktime(_time) as time | table _time, time, USER, PID, PSR, pctCPU, CPUTIME, pctMEM, RSZ, VSZ, S, ELAPSED, COMMAND, ARGS',
                fields: ['PID', 'pctCPU', 'pctMEM', 'RSZ', 'VSZ']
            }
        };
    
        var scaffolds = [
            {
            id: 'PS',
            ctype: indentedTree,
            title: 'PS',
            config: {
                source: 'ps',
                order: ['USER', 'PID', 'COMMAND', 'ARGS'],
                fields: ['pctCPU'/*, 'CPUTIME'*/, 'pctMEM', 'RSZ', 'VSZ'/*, 'ELAPSED'*/],
                keysorts: {
                    //'USER': d3.descending,
                    //'PID': d3.descending,
                    'pctCPU': function(d) {}
    /*,
                                'CPUTIME': {
                                },
                                'pctMEM': {
                                },
                                'RSZ': {
                                },
                                'VSZ': {
                                },
                                'ELAPSED': {
                                },
                                'COMMAND': {
                                },
                                'ARGS': {
                                }*/
                },
                width: 800,
                height: 800,
                barheight: 20
            }}
        ];
    
        // create chart skeletons
        skeleton();
    
        collect_metrics(d);
    
        function skeleton() {
            for (var i = 0; i < scaffolds.length; i++) {
                var scaf = scaffolds[i],
                    s;
    
                s = scaf.ctype(scaf.config).margin(margin).title(scaf.title);
    
                scaf.chart = s;
            }
    
            var chart = div.selectAll(".chart").data(scaffolds).enter().append('div').attr('data-simplechart-id', function(d) {
                return d.id;
            }).attr('class', 'CFScrollerXSimple chart');
    
            //renderAll();
            // Renders the specified chart or list.
    
            function render(d) {
                d3.select(this).call(d.chart);
            }
    
            // re-rendering everything.
    
            function renderAll() {
                chart.each(render);
            }
        }
    
        function collect_metrics(d) {
            d.metrics = d.metrics || {};
            //get_events(d);
            for (var k in base)
                get_metric(d, k);
        }
    
        function get_metric(d, stype) {
            if (d.metrics[stype] === undefined) return;
    
            render_metrics(stype);
        }
    
        function render_metrics(type) {
            var scafs = scaffolds.filter(function(s) {
                return s.config.source === type;
            });
    
            for (i = 0; i < scafs.length; i++) {
                var scaf = scafs[i],
                    chart = scaf.chart,
                    data = d.metrics[type];
    
                chart.data(data);
                //chart.order(scaf.config.order);
                // render
                div.select('[data-simplechart-id=' + scaf.id + ']').call(chart);
            }
        }
    }
});
