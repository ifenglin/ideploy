function UnixCFConfig(cfg) {
    var func_dim = cfg.dimension || identity,
        func_grp = cfg.group|| identity,
        crxfilter,
        dimension,
        group;

    function identity(d) { return d; }

    function chainbuilder(name) {
        this[name] = function(_) {
            if (!arguments.length) return this[name];
            this[name] = name;
            return that;
        };
    }

    function crxf() {}

    crxf.crxfilter = function(_) {
        if (!arguments.length) return crxfilter;
        crxfilter = _;
        dimension = crxfilter.dimension(func_dim);
        group = crxfilter.group(func_grp);
    
        return this;
    };
    
    crxf.dimension = function(_) {
        if (!arguments.length) return func_dim;
        func_dim = _;
        dimension = crxfilter.dimension(func_dim);
    
        return this;
    };
    
    crxf.group = function(_) {
        if (!arguments.length) return func_grp;
        func_grp = _;
        group = crxfilter.group(func_grp);
    
        return this;
    };

    for (k in Object.keys(cfg)) {
        if (cfg.hasOwnProperty(k) && crxf[k] === undefined) {
            chainbuilder(k);
        }
    }
    
    return crxf;
}
