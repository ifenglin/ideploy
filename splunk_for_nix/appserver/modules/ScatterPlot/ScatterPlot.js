Splunk.Module.ScatterPlot = $.klass(Splunk.Module, {
    initialize: function ($super, container) {


        $super(container);

        this.div_id = '#' + this.moduleId + '_scatterplot';
        this.svg_id = this.moduleId + '_svg';


    },


    displayScatterPlot: function (data) {
    
    	
		 //remove original chart before redraw
        //d3.select("#" + this.svg_id+).remove();

        this.div = d3.select(this.div_id);
    
        this.div.selectAll("svg").remove();

        var flowers = d3.csv.parse(data);

        // Size parameters.
        var padding = 10, //may adjust according to outer width	    	
            traits = getMetricArray(),
            hosts = getHostArray(),
            n = traits.length,
            size = 600 / n,
            hostColor = getHostColor(); //this need to load from the param ;

        convertData();

        //convert the search result to desired format that can be used for rendering     
        function convertData() {

            var data = [];

            var temp;

            // 					
            for (var i = 0; i < flowers.length; i++) {


                for (var j = 0; j < hosts.length; j++) {

                    temp = new Object();

                    temp["species"] = hosts[j];

                    for (var k = 0; k < traits.length; k++) {

                        temp[traits[k]] = flowers[i]["@" + traits[k] + "@" + hosts[j]];
                    }

                    data.push(temp);
                }

            }

            flowers = data;

        }

        function getHostArray() {

            var row = flowers[0];
            var hosts = [];
            var oneHost;

            for (var name in row) {
                if (name && name.charAt(0) == "@") {

                    oneHost = name.split("@")[2];

                    if (hosts.indexOf(oneHost) < 0) {
                        hosts.push(oneHost);
                    }
                }
            }

            return hosts;
        }

        function getMetricArray() {

            var row = flowers[0];
            var metrics = [];
            var oneMetric;

            for (var name in row) {
                if (name && name.charAt(0) == "@") {

                    oneMetric = name.split("@")[1];

                    if (metrics.indexOf(oneMetric) < 0) {
                        metrics.push(oneMetric);
                    }
                }
            }
            return metrics;
        }

        function getHostColor() {

            var colors = {};

            for (var i = 0; i < hosts.length; i++) {

                //generate random color for each host
                colors[hosts[i]] = '#' + Math.floor(Math.random() * 16777215).toString(16);

            }

            return colors;
        }



        // Position scales.
        var x = {}, y = {};
        traits.forEach(function (trait) {
            var value = function (d) {
                return d[trait];
            },
            //domain = [d3.min(flowers, value), d3.max(flowers, value)],
            domain = [0,100],
                range = [padding / 2, size - padding / 2];
            x[trait] = d3.scale.linear().domain(domain).range(range);
            y[trait] = d3.scale.linear().domain(domain).range(range);
        });

        // Axes.
        var axis = d3.svg.axis()
            .ticks(5)
            .tickSize(size * n);

        // Brush.
        var brush = d3.svg.brush()
            .on("brushstart", brushstart)
            .on("brush", brush)
            .on("brushend", brushend);

        // Root panel.
        var svg = this.div.append("svg:svg")
            .attr("width", 1280)
            .attr("height", 800)
            .append("svg:g")
            .attr("transform", "translate(359.5,69.5)");

        // Legend.
        var legend = svg.selectAll("g.legend")
            .data(hosts) // may leave it off or put it on top of it 
        .enter().append("svg:g")
            .attr("class", "legend")
            .attr("transform", function (d, i) {
            return "translate(-179," + (i * 20 + 594) + ")";
        });

        legend.append("svg:circle")
            .attr("class", String)
            .attr("r", 3)
            .attr("style", function (d) {
            return "fill:" + hostColor[d];
        });

        legend.append("svg:text")
            .attr("x", 12)
            .attr("dy", ".31em")
            .text(function (d) {
            return d;
        });

        // X-axis.
        svg.selectAll("g.x.axis")
            .data(traits)
            .enter().append("svg:g")
            .attr("class", "x axis")
            .attr("transform", function (d, i) {
            //alert("i: " + i + " size:" + size);
            return "translate(" + i * size + ",0)";
        })
            .each(

        function (d) {
            d3.select(this).call(axis.scale(x[d]).orient("bottom"));
        });


        // Y-axis.
        svg.selectAll("g.y.axis")
            .data(traits)
            .enter().append("svg:g")
            .attr("class", "y axis")
            .attr("transform", function (d, i) {
            return "translate(0," + i * size + ")";
        })
            .each(function (d) {
            d3.select(this).call(axis.scale(y[d]).orient("right"));
        });

        // Cell and plot.
        var cell = svg.selectAll("g.cell")
            .data(cross(traits, traits))
            .enter().append("svg:g")
            .attr("class", "cell")
            .attr("transform", function (d) {
            return "translate(" + d.i * size + "," + d.j * size + ")";
        })
            .each(plot);

        // Titles for the diagonal.
        cell.filter(function (d) {
            return d.i == d.j;
        }).append("svg:text")
            .attr("x", padding)
            .attr("y", padding)
            .attr("dy", ".71em")
            .text(function (d) {
            return d.x;
        });

        function plot(p) {
            var cell = d3.select(this);

            // Plot frame.
            cell.append("svg:rect")
                .attr("class", "frame")
                .attr("x", padding / 2)
                .attr("y", padding / 2)
                .attr("width", size - padding)
                .attr("height", size - padding);

            // Plot dots.
            cell.selectAll("circle")
                .data(flowers)
                .enter().append("svg:circle")
                .attr("class", function (d) {
                return d.species;
            })
                .attr("cx", function (d) {
                return x[p.x](d[p.x]);
            })
                .attr("cy", function (d) {
                return y[p.y](d[p.y]);
            })
                .attr("r", 3)
                .attr("style", function (d) {
                return "fill:" + hostColor[d.species];
            });

            // Plot brush.
            cell.call(brush.x(x[p.x]).y(y[p.y]));
        }

        // Clear the previously-active brush, if any.
        function brushstart(p) {
            if (brush.data !== p) {
                cell.call(brush.clear());
                brush.x(x[p.x]).y(y[p.y]).data = p;
            }
        }

        // Highlight the selected circles.
        function brush(p) {
            var e = brush.extent();
            svg.selectAll(".cell circle").attr("style", function (d) {
                return e[0][0] <= d[p.x] && d[p.x] <= e[1][0] && e[0][1] <= d[p.y] && d[p.y] <= e[1][1] ? "fill:" + hostColor[d.species] : null;
            });
        }

        // If the brush is empty, select all circles.
        function brushend() {
            if (brush.empty()) svg.selectAll(".cell circle").attr("style", function (d) {
                return "fill:" + hostColor[d.species];
            });
        }

        function cross(a, b) {
            var c = [],
                n = a.length,
                m = b.length,
                i, j;
            for (i = -1; ++i < n;) for (j = -1; ++j < m;) c.push({
                x: a[i],
                i: i,
                y: b[j],
                j: j
            });
            return c;
        }

    },

    

    onContextChange: function () {

		//alert("onContextChange");

        var that = this,
            context = this.getContext(),
            namespace = context.get('namespace'),
            nsobj = context.get(namespace),
            dataset = context.get('dataset'),
            name = this.varname;

        if (dataset != undefined) {
            this.displayScatterPlot(dataset.results);
        }
    }

});