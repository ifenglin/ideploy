"""
Meta
====
    $Id: //splunk/solutions/nix/mainline/test/common/NixUtil.py#12 $
    $DateTime: 2011/03/20 23:04:29 $
    $Author: dzakharov $
    $Change: 96913 $
"""


import os
import sys
import shutil
import glob
import zipfile
import tarfile
import logging
from SplunkCLI import SplunkCLI
from BuildUtil import BuildUtil
from InstallUtil import InstallUtil
from LicenseUtil import LicenseUtil
from SplunkConf import SplunkConfManager

class NixUtil:

    def __init__(self, splunk_home, logger):
        """
        Constructor of the NixUtil object.
        """
        self.logger = logger
        self.splunk_home = splunk_home
        self.logger.info("sys.path: " + str(sys.path))
        self.splunk_cli = SplunkCLI(self.splunk_home)
        self.SplunkConfManager = SplunkConfManager(splunk_home=self.splunk_home)

    def get_nix(self):
        #get Nix
        install_util = InstallUtil("unix", self.splunk_home, self.logger)
        package = install_util.get_solution()

        self.logger.info("Nix file:" + package)
        return package

    def get_and_install_nix(self):

        self.soln_root = os.environ["SOLN_ROOT"]
        self.logger.info("SOLN_ROOT:" + self.soln_root)
        self.logger.info("Installing License if on Linux (does not work on Windows)")
        if not sys.platform.startswith("win"):
            self.license_util = LicenseUtil(self.logger)
            self.license_util.add_5tb_license()
                   
        #install Nix
        package = self.get_nix()

        self.logger.info("Nix file:" + package)
        
        zipPackage = zipfile.ZipFile(package)

        # unpack nix package
        zipPackage.extractall(path=".")

        # copy nix to Splunk
        apps_dir = os.path.join(self.splunk_home, 'etc', 'apps')
        for fd in glob.glob(os.path.join('etc', 'apps', '*')):
            copyto_dir = os.path.join(apps_dir, fd[9:])
            if os.path.isdir(fd) == True:
                shutil.copytree(fd, copyto_dir)
            else:
                shutil.copyfile(fd, copyto_dir)

        deployment_apps_dir = os.path.join(self.splunk_home, 'etc', 'deployment-apps')
        for fd in glob.glob(os.path.join('etc', 'deployment-apps', '*')):
            copyto_dir = os.path.join(deployment_apps_dir, fd[20:])
            if os.path.isdir(fd) == True:
                shutil.copytree(fd, copyto_dir)
            else:
                shutil.copyfile(fd, copyto_dir)

        # cleanup
        # delete package in test code
        #os.remove(package)
        shutil.rmtree("etc", ignore_errors=True)
 
        self.logger.info("Starting splunk.")
        
        # the start method will throw exception if it doesn't succeed
        self.splunk_cli.restart('--accept-license')
        
        self.logger.info("Started splunk.")

        return package
