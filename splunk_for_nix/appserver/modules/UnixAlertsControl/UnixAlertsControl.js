Splunk.Module.UnixAlertsControl = $.klass(Splunk.Module, { 

    initialize: function ($super, container) {
        $super(container);

        this.namespace = this.getParam('namespace');
        this.token = this.getParam('token');

        this.$control = $('a#interval_control', this.container);

        this.initBindings();
    },

    getToken: function() {
        return this.token;
    },

    onContextChange: function() {
        var context = this.getContext(),
            value = context.get(this.namespace + '.' + this.token) || null;

        if (value !== null&& value !== undefined) {
            if (value === 'pause') {
                this.$control.removeClass('play')
                    .addClass('pause')
                    .text('Resume');
            } else {
                this.$control.removeClass('pause')
                    .addClass('play')
                    .text('Pause');
            }
        }
    },

    getControl: function() {
        var output;

        if (this.$control.hasClass('play') === true) {
            output = 'play';
        } else {
            output = 'pause';
        }

        return output;
    },

    getModifiedContext: function() {
        var context = this.getContext();

        context.set(this.namespace + '.' + this.token, this.getControl());

        return context;
    },

    initBindings: function() {
        var that = this;

        this.$control.click(function(e) {
            var $elm = $(this);
            e.preventDefault();
            if ($elm.hasClass('play') === true) {
                $elm.removeClass('play')
                    .addClass('pause')
                    .text('Resume');
            } else {
                $elm.removeClass('pause')
                    .addClass('play')
                    .text('Pause');
            }
            that.pushContextToChildren();
        });

    }

});

