# Copyright 2012 Splunk, Inc.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.from splunk.models.base import SplunkRESTModel, SplunkAppObjModel

from splunk.models.base import SplunkRESTModel, SplunkAppObjModel
from splunk.models.field import Field, BoolField, IntField, ListField
import splunk.rest as rest
import logging

logger = logging.getLogger('splunk.models.savedsearch')

'''
Provides object mapping for savedsearch objects
'''

class SavedSearch(SplunkAppObjModel):

    resource              = 'saved/searches'
    disabled              = BoolField(is_mutable=False)
    alert_track           = BoolField(api_name="alert.track")

    def _reload(self):
        path = '/'.join([self.id.rsplit('/', 1)[0], '_reload'])
        response, content = rest.simpleRequest(path,
                                 method='POST')
        if response.status == 200:
            return True
        return False

    def enable(self):
        if not self.action_links:
            return True
        for item in self.action_links:
            if 'enable' in item:
                response, content = rest.simpleRequest(item[1],
                                         method='POST')
                if response.status == 200:
                    return True
        return False

#    def disable(self):
#        if not self.action_links:
#            return True
#        for item in self.action_links:
#            if 'disable' in item:
#                response, content = rest.simpleRequest(item[1],
#                                         method='POST')
#                if response.status == 200:
#                    return True
#        return False
#
#    def delete(self):
#        if not self.action_links:
#            return False
#        for item in self.action_links:
#            if 'remove' in item:
#                response, content = rest.simpleRequest(item[1],
#                                         method='DELETE')
#                if response.status == 200:
#                    return True
#        return False
