Splunk.Module.BulletChart = $.klass(Splunk.Module, {
    initialize: function ($super, container) {
        
        $super(container);

        this.div_id = '#' + this.moduleId + '_bulletChart';
        this.svg_id = this.moduleId + '_svg';
        $script(Splunk.util.make_url('/modules/BulletChart/BulletChart.v1.js'), 'bullet_js');
    },
    
    displayChart: function(data){
        var div_id = this.div_id;
        
        var margin = {top: 5, right: 5, bottom: 20, left: 120},
        width = 300 - margin.left - margin.right,
        height = 40 - margin.top - margin.bottom;
    
        var chart = d3.bullet()
            .width(width)
            .height(height);
        
        var div_bulletChart = d3.select(div_id);
        
        div_bulletChart = div_bulletChart.html('');
        
        
        if(div_bulletChart.style('width') == '0') {
            div_bulletChart.style('width', '1020px')
                .style('height', 'auto');
        }
        
        div_bulletChart.append('div')
            .attr('class', 'close_bulletChart')
            .text('X');
            
        min_bulletChart = div_bulletChart.append('div')
                         .attr('class', 'min_bulletChart');
        
        if (div_bulletChart.attr('status') == undefined) {
                div_bulletChart.attr('status', 'M');
                min_bulletChart.text('m');
        } else if (div_bulletChart.attr('status') == 'm') { 
            min_bulletChart.text('M');
        } else { 
            min_bulletChart.text('m');
        }
                         
            
        var svg_bulletChart = d3.select(div_id).selectAll("svg");
        
        for(var host in data){
            
            var data_by_host = data[host];
            
            var div_bulletChart_byHost = div_bulletChart.append("div");
            
            div_bulletChart_byHost.append("text")
                .attr("class", "hostname")
                .text(host);
                
            var svg_bulletChart_byHost = svg_bulletChart.append("g")
                .data(data_by_host)
                .enter().append("svg")
                .attr("class","bullet")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
                .call(chart);
                
            var title = svg_bulletChart_byHost.append("g")
                .style("text-anchor", "end")
                .attr("transform", "translate(-6," + height / 2 + ")");
          
            title.append("text")
                .attr("class", "title")
                .text(function(d) { return d.title; });
          
            title.append("text")
                .attr("class", "subtitle")
                .attr("dy", "1em")
                .text(function(d) { return d.subtitle; });
        
        }
        
        //jQuery
        $('.close_bulletChart')
            .click(function(event){
                $(div_id).html('');
        });
        $(div_id).draggable().draggable('disable').removeClass('ui-state-disabled');
        $('.min_bulletChart')
            .click(function(event){
                $div = $(div_id);
                if( $div.attr('status') == 'M' ) {
                    $div.animate(
                        { width: 300 },
                        600,
                        function() {
                            $div.attr('status', 'm');
                            $('.min_bulletChart').text('M');
                            $div.draggable('enable');
                        }
                    );
                } else {
                    $(div_id).animate(
                        { width: 1020, left: 0, top: 0 },
                        600,
                        function() {
                            $div.attr('status', 'M');
                            $('.min_bulletChart').text('m');
                            $div.draggable('disable').removeClass('ui-state-disabled');
                        }
                    );
                    
                }
        });
        if (svg_bulletChart == undefined){
            div_bulletChart.style("display", "none");
        } else {
            div_bulletChart.style("display", "block");
        }
        
     },
    
    getDataByProcess: function(data, indexOnClick){
        var new_data = new Object();
        var items = {}, val = [];
        var timeOnClick, value, colon_split, at_split, type, ps, host;
        data = d3.csv.parse(data);
        if (data[indexOnClick] != undefined){
            timeOnClick = data[indexOnClick]['_time'];
        }
        data.forEach(function(obj) {
            for (var p in obj) {
                if (obj.hasOwnProperty(p)) {
                    if(p=='_time' && obj[p] == timeOnClick){ //store the data at time point
                        for (var q in obj) {
                            if (obj.hasOwnProperty(q) && q.charAt(0)=='#') {
                                val[q.slice(1)] = parseFloat(obj[q]);
                            }
                        }
                    }
                    else if(p.charAt(0)=='#'){
                        value = ( obj[p] == undefined || obj[p] == "" || obj[p] == null) ? 0.0: parseFloat(obj[p]);
                        colon_split = p.split(': ');
                        at_split = colon_split[1].split('@');
                        type = colon_split[0].slice(1);
                        ps = at_split[0];
                        host = at_split[1];
                        if(items.hasOwnProperty(host) && items[host]['data'].hasOwnProperty(ps) && items[host]['data'][ps].hasOwnProperty(type)){
                            items[host]['data'][ps][type].avg += value;
                            items[host]['data'][ps][type].num++;
                            if (value > items[host]['data'][ps][type].max){
                                items[host]['data'][ps][type].max = value;
                            }
                        } else {
                            if(!items.hasOwnProperty(host)){
                                items[host] = {};
                                items[host]['_max_overall'] = 0.0;
                                items[host]['data'] = {};
                            }
                            if(!items[host]['data'].hasOwnProperty(ps)){
                                items[host]['data'][ps]= {};
                            }
                                items[host]['data'][ps]['pctCPU'] = {
                                    'avg': value,
                                    'max': value,
                                    'num': 1
                                    };
                                items[host]['data'][ps]['pctMEM'] = {
                                    'avg': value,
                                    'max': value,
                                    'num': 1
                                    };
                        }
                    }
                }
            }
        });
        
        var val_cpu, val_mem;
        for (var host in items){
            for (var ps in items[host]['data']){
                val_cpu = val['pctCPU: '+ps+'@'+host] == undefined? 0: val['pctCPU: '+ps+'@'+host];
                val_mem = val['pctMEM: '+ps+'@'+host] == undefined? 0: val['pctMEM: '+ps+'@'+host]
                if (val_cpu == 0 && val_cpu == 0 ){
                    continue;
                }
                for(var type in items[host]['data'][ps]){
                    if (items[host]['data'][ps][type].max > items[host]['_max_overall']){
                        items[host]['_max_overall'] = items[host]['data'][ps][type].max;
                    }
                    items[host]['data'][ps][type].avg /= items[host]['data'][ps][type].num;
                }
            }
        }
        
        var item;
        var new_data = {};
        for (var host in items){
            if(!new_data.hasOwnProperty(host))
             new_data[host] = [];
            for (var ps in items[host]['data']){
                val_cpu = val['pctCPU: '+ps+'@'+host] == undefined? 0: val['pctCPU: '+ps+'@'+host];
                val_mem = val['pctMEM: '+ps+'@'+host] == undefined? 0: val['pctMEM: '+ps+'@'+host]
                if (val_cpu == 0 && val_cpu == 0 ){
                    continue;
                }
                item = new Object();
                item.title = ps;
                item.subtitle = 'cpu='+val_cpu+'%, mem='+val_mem+'%'
                item.ranges = [ val_mem, items[host]['data'][ps]['pctMEM'].max, items[host]['_max_overall'] ];
                item.measures = [ val_cpu, items[host]['data'][ps]['pctCPU'].max];
                item.markers = [ items[host]['data'][ps]['pctCPU'].avg ];
                new_data[host].push(item);
            }
        }
        
        return new_data;
    },

  
    getModifiedContext: function() {
    
        var that = this,
            context = this.getContext(),
            dataset = context.get('dataset');
    },

    onContextChange: function () { 
        
         var that = this,
            context = this.getContext(),
            dataset = context.get('dataset');

        var indexOnClick = context.get('indexOnClick');
        if (dataset != undefined) {
            this.displayChart(this.getDataByProcess(dataset.results,indexOnClick));
        }
    }
    

});