Splunk.Module.CFScrollerXSimple = $.klass(Splunk.Module, {

    initialize: function($super, container) {
        $super(container);

        this.hide(this.HIDDEN_MODULE_KEY);
        this.logger = Splunk.Logger.getLogger("cf_scrollerx_simple.js", Splunk.Logger.mode.console);
        this.tracker = {};
        this.base = {
                cpu: {
                    basesearch: 'index=os sourcetype=cpu host=$HOST$ | multikv | search CPU="all" | convert mktime(_time) as time | table _time, time, host, sourcetype, pctUser, pctSystem',
                    fields: ['pctUser', 'pctSystem']
                },
                vmstat: {
                    basesearch: 'index=os sourcetype=vmstat host=$HOST$ | multikv | convert mktime(_time) as time | eval memUsed=memUsedMB*1000000 | eval memFree=memFreeMB*1000000 | table _time, time, sourcetype, host, memFreePct, memUsedPct, memFree, memUsed, processes, threads',
                    fields: ['memFreePct', 'memUsedPct', 'memFree', 'memUsed']
                }
            };
    },

    onContextChange: function($super) {
        var context = this.getContext(),
            data = context.get('cfscroller_data'),
            base = this.base,
            that = this;

        if (!data)
            return;
        
        if (data.selection === undefined) {
            this.logger.log("incomplete cfscroller data", data);
            return;
        }

        if (data.level === 1)
            process_cf_data(data);

        function process_cf_data(data) {
            var div = data.selection,
                d = div.data()[0],
                colors = d3.scale.category10().domain(
                    data.group.all().map(function(x) { return x.key; })),
                severity = d.severity,
                tags = d.tags || 'untagged',
                time = new Date(Date.parse(d._time)),
                margin = {top: 5, right: 0, bottom: 30, left: 30},
                width = +div.style('width').slice(0, -2) - margin.left - margin.right,
                height = 80 - margin.top - margin.bottom;

            var scaffolds = [
                {id: 'CPU_Usage',
                    ctype: stackedArea,
                    title: 'CPU Usage',
                    config: {
                        source: 'cpu',
                        x: d3.time.scale()
                            .range([0, width]),
                        y: d3.scale.linear()
                            .range([height, 0]),
                        x_domain: 'date',
                        layers: ['pctUser', 'pctSystem'],
                        width: width,
                        height: height,
                        y_axis: {ticks: [3]}
                    }
                },
                {id: 'Mem_Used_Pct',
                    ctype: lineChart,
                    title: 'Memory Used %',
                    config: {source: 'vmstat',
                        x: d3.time.scale().range([0, width]),
                        y: d3.scale.linear().range([height, 0]),
                        x_domain: 'date',
                        y_domain: 'memUsedPct',
                        y_axis: {ticks: [3]}
                    }
                },
                {id: 'Process_Count',
                    ctype: lineChart,
                    title: 'Process #',
                    config: {source: 'vmstat',
                        x: d3.time.scale().range([0, width]),
                        y: d3.scale.linear().range([height, 0]),
                        x_domain: 'date',
                        y_domain: 'processes',
                        y_axis: {ticks: [3]}
                    }
                },
                {id: 'Thread_Count',
                    ctype: lineChart,
                    title: 'Thread #',
                    config: {source: 'vmstat',
                        x: d3.time.scale().range([0, width]),
                        y: d3.scale.linear().range([height, 0]),
                        x_domain: 'date',
                        y_domain: 'threads',
                        y_axis: {ticks: [3], tickFormat: [d3.format('s')]}
                    }
                }
            ];

            // header
            var header = div.append('div')
                .attr('class', 'CFScrollerXSimple excerpt-header')
                .attr('data-expand', '')
                .style('background-color', colors(d.ss_name));

            header.append('div')
                .attr('class', 'CFScrollerXSimple severity severityMedium')
                .text(d.ss_name);

            header.append('div')
                .attr('class', 'CFScrollerXSimple alertinfo')
              .selectAll('div')
                .data([d3.time.format("%a %b %e %Y %H:%M:%S")(time), d.host])
              .enter().append('div')
                .text(function(d) { return d; });

            // content
            var content = div.append('div')
                .attr("class", "CFScrollerXSimple excerpt-content");

            // create chart skeletons
            skeleton();

            // get alert events
            get_alert_job(d);

            function skeleton() {
                var i;
                for (i = 0; i < scaffolds.length; i++) {
                    var scaf = scaffolds[i],
                        s;
                        
                    s = scaf.ctype(scaf.config)
                        .margin(margin)
                        .title(scaf.title);

                    scaf.chart = s;
                }

                var chart = content.selectAll(".chart")
                    .data(scaffolds)
                  .enter().append('div')
                    .attr('data-simplechart-id', function(d) { return d.id; })
                    .attr('class', 'CFScrollerXSimple chart');

                //renderAll();

                // Renders the specified chart or list.
                function render(d) {
                    d3.select(this).call(d.chart);
                }
                
                // re-rendering everything.
                function renderAll() {
                    chart.each(render);
                }
            }

            function get_alert_job(d) {
                var basesearch = '| rest /services/search/jobs search=sid=' + d.sid;

                if (!(basesearch in that.tracker)) {
                    var timerange = new Splunk.TimeRange('0', 'now');

                    that.tracker[basesearch + "," + timerange.toString()] = false;

                    that._stealthSearch({'basesearch': basesearch, 'timerange': timerange}, function(response) {
                        if (!arguments.length || 
                            response.result === undefined || response.result === null ||
                            response.result.length < 1) {
                            that.logger.error("empty response");
                            return;
                        }

                        var result = response.result[0];

                        d.ss_culprit = result;
                        result.earliestTime = Date.parse(result.earliestTime);
                        result.latestTime = Date.parse(result.latestTime);
                        result.timerange = new Splunk.TimeRange(result.earliestTime/1000, result.latestTime/1000);

                        collect_metrics(d);
                    });
                }
            }

            function collect_metrics(d) {
                var k;

                d.metrics = d.metrics || {};
                //get_events(d);
                for (k in base)
                    get_metric(d, k);
            }

            function get_events(d) {
                var job = d.ss_culprit;
                that._stealthSearch({'basesearch': job.title, 'timerange': job.timerange}, function(response) {
                    d.ss_events = response.result;
                });
            }

            function get_metric(d, stype) {
                if (d.metrics[stype]) {
                    render_metrics(stype);
                    return;
                }

                var job = d.ss_culprit,
                    spec = base[stype],
                    basesearch = spec.basesearch,
                    fields = spec.fields;

                that._stealthSearch({'basesearch': basesearch, 'timerange': job.timerange}, function(response) {
                    var result = response.result;

                    result.forEach(function(d) {
                        d.time = +d.time;
                        d.date = new Date(d.time * 1000);
                    
                        for (i = 0; i < fields.length; i++) {
                            d[fields[i]] = +d[fields[i]];
                        }
                    });
                
                    d.metrics[stype] = result;
                    render_metrics(stype);
                },
                {HOST: d.host});
            }

            function render_metrics(type) {
                // TODO: make this update like the 'Events count' above?
                var scafs = scaffolds.filter(function(s) {
                    return s.config.source === type;
                });

                for (i = 0; i < scafs.length; i++) {
                    var scaf = scafs[i],
                        chart = scaf.chart,
                        data = d.metrics[type];

                    chart.data(data);

                    // render
                    content.select('[data-simplechart-id=' + scaf.id + ']').call(chart);
                }
            }
        }
    },

    resetUI: function() {
        this.container.empty();
    },

    _stealthSearch: function(sobj, callback, values) {
        var basesearch = sobj && sobj.basesearch,
            timerange = sobj.timerange || new Splunk.TimeRange('0', 'now'),
            that = this;

        if (basesearch === undefined || basesearch === null || basesearch === '') {
            this.logger.error('no search string');
            return;
        }

        if (typeof callback !== 'function') {
            values = callback || {};
            callback = null;
        } else
            values = values || {};

        var search = new Splunk.Search(),
            tokens = Splunk.util.discoverReplacementTokens(basesearch),
            i;

        for (i = 0; i < tokens.length; i++) {
            var replacer = new RegExp("\\$" + tokens[i] + "\\$");
            basesearch = Splunk.util.replaceTokens(basesearch, replacer, values[tokens[i]]);
            this.logger.log('token ' + tokens[i] + ' replaced with [' + values[tokens[i]] + ']');
        }

        search.setBaseSearch(basesearch);
        search.setTimeRange(timerange);

        this.logger.debug("stealth search:", search);

        search.dispatchJob(
            function(search) {
                var _getResults = function () {
                    var counter = 0,
                        params = {sid: search.job.getSearchId()},
                        resultUrl = this.getResultURL(params),
                        callingModule = this.moduleId,
                        xhrObject,
                        xhrRetryCount = 10;

                    if (!search.job.isDone()) {
                        if (counter > xhrRetryCount) {
                            xhrObject.abort();
                            xhrObject = null;
                            this.logger.info('XHR in-flight destroyed for module', callingModule, 'for job', job.getSearchId(), 'and replaced with new one');
                        } else {
                            counter++;
                            setTimeout(_getResults, 250);
                        }
                        return;
                    }

                    this.logger.info('XHR clear for takeoff for module', callingModule, search.job.getSearchId());

                    xhrObject = $.ajax({
                        type: "GET",
                        //cache: ($.browser.msie ? false : true),
                        cache: false,
                        url: resultUrl,
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('X-Splunk-Module', callingModule);
                        },
                        success: function(htmlFragment, textStatus, xhr) {
                            that.logger.debug('->XHR DEBUG: observer: success status:', xhr.status, 'module:', callingModule, 'responseText:', !!(htmlFragment));
                            //JQuery 1.4 bug where success callback is called after an aborted request
                            //NOTE: status 0 means the resource is unreachable
                            if (xhr.status === 0) {
                                return;
                            }

                            that.tracker[search.getBaseSearch() + "," + search.getTimeRange().toString()] = true;
                            
                            if (callback)
                                callback.call(this, htmlFragment);
                        }.bind(this),
                        complete: function(xhr, textStatus) { this.xhrObject = null; },
                        error: function(xhr, textStatus, errorThrown) {
                            //xhr = null;
                            if (textStatus === 'abort') {
                                that.logger.debug(that.moduleType, '.getResults() aborted');
                            } else {
                                that.logger.warn(that.moduleType, '.getResults() error; textStatus=' + textStatus + ' errorThrown=' + errorThrown);
                            }
                            this.logger.error(textStatus, errorThrown);
                        }
                    });
                }.bind(this);

                _getResults();
            }.bind(this),

            function(search) {
                that.logger.error(this.moduleType, " Context failed to dispatch job for search=", search.toString());
            }
        );
    }
});
